package com.sportnetic.repositories;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Transient;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Data;


@Data
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public class BaseAuditTrailEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name = "CREATED_BY", updatable = false)
	@CreatedBy
	protected String createdBy;

	@Column(name = "CREATED_DATE", updatable = false)
	@CreatedDate
	protected Date createdDate;

	@Column(name = "MODIFIED_BY")
	@LastModifiedBy
	protected String modifiedBy;
	
	@Column(name = "DESCRIPTION")
	protected String description;

	@Column(name = "MODIFIED_DATE")
	@LastModifiedDate
	protected Date modifiedDate;

	@Column(name = "IS_ACTIVE")
	protected Boolean isActive = true;
	
	@Transient
	protected String overrideModifiedBy;
	
	@Transient
	protected String overrideCreatedBy;
	
	@PrePersist
	@PreUpdate
	public void overrideModifiedBy() {
		if(overrideModifiedBy!=null)
			modifiedBy = overrideModifiedBy;
		if(overrideCreatedBy!=null)
			createdBy = overrideCreatedBy;
	}
	

}
