package com.sportnetic.repositories.master.specification;

import java.util.Map;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import com.sportnetic.utils.*;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.data.jpa.domain.Specification;

import com.sportnetic.repositories.master.entity.UserEntity;
import com.sportnetic.utils.FilterSpec;

public class UserSpecification {

	private static Map<String, FilterSpec> p_filter2;

	public static Specification<UserEntity> getUserSpec(Map<String, FilterSpec> p_filter) {
		p_filter2 = p_filter;

		return new Specification<UserEntity>() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			public Predicate toPredicate(Root<UserEntity> root, CriteriaQuery<?> criteria, CriteriaBuilder builder) {
				Predicate _predicate = builder.disjunction();
				if (p_filter2 == null || p_filter2.isEmpty())
					_predicate = builder.conjunction();
				else {
					for (Map.Entry<String, FilterSpec> _field : p_filter2.entrySet()) {
						String key = _field.getKey();
						FilterSpec _filterSpec = (_field.getValue() == null) ? null : _field.getValue();
						try {
							if (_filterSpec.getValues() != null) {
								QueryOperator operator = (_field.getValue().getOperator() == null) ? null
										: _field.getValue().getOperator();
								String _value = (_field.getValue().getValues() == null) ? null
										: _field.getValue().getValues()[0].toString();
								switch (operator) {
								case EQUAL:
									if (_value == null)
										_predicate.getExpressions().add(builder.isNull(root.get(key)));
									else
										_predicate.getExpressions().add(builder.equal(root.get(key), _value));
									break;
								case LIKE_BOTH_SIDE:
									switch (key) {
									case "email":
									case "nama":
									case "gender":
									case "createdBy":
										_predicate.getExpressions().add(builder.like(root.<String>get(key),
												String.format(operator.getOperator(), _value)));
										break;
									
									default:
										break;
									}
									break;
								default:
									break;
								}
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}
				return _predicate;
			}
		};
	}

	private static final String IS_ACTIVE = "isActive";
	private static final String FORMAT_DATE = "dd-MM-yyyy";

	private static Date[] getStartDateAndEndDate(String p_date) throws Exception {
		Date _startDate = new SimpleDateFormat(FORMAT_DATE).parse(p_date);
		Calendar _calendar = Calendar.getInstance();
		_calendar.setTime(_startDate);
		_calendar.add(Calendar.SECOND, 86399);
		return new Date[] { _startDate, _calendar.getTime() };
	}
}
