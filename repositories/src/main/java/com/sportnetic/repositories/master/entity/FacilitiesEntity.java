package com.sportnetic.repositories.master.entity;

import java.io.Serializable;
import java.util.List;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.sportnetic.repositories.BaseAuditTrailEntity;

import lombok.Data;

@Entity
@Data
@Table(name = "M_FACILITY", schema = "SPORT_NETIC_MASTER")
public class FacilitiesEntity extends BaseAuditTrailEntity implements Serializable {
	/**
	* 
	*/
	private static final long serialVersionUID = 2644033823801514250L;

	@Id
	@GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
	@GeneratedValue(generator = "UUID")
	@Column(name = "FACILITIES_ID", nullable = false, unique = true)
	private UUID facilitiesId;

	@Column(name = "PARKING_AREA", nullable = false)
	private Boolean parkingArea;

	@Column(name = "WAITING_AREA", nullable = false)
	private Boolean waitingArea;

	@Column(name = "TOILET", nullable = false)
	private Boolean toilet;

	@Column(name = "PANTRI", nullable = false)
	private Boolean pantri;
	
	@Column(name = "MUSHOLAH", nullable = false)
	private Boolean musholah;
	
	@Column(name = "OTHER", nullable = false)
	private Boolean other;
}
