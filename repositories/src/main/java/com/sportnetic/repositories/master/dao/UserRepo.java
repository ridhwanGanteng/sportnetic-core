package com.sportnetic.repositories.master.dao;

import java.util.UUID;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.repository.PagingAndSortingRepository;


import com.sportnetic.repositories.master.entity.UserEntity;

public interface UserRepo extends PagingAndSortingRepository<UserEntity, UUID> {

	UserEntity findByUserId(UUID uuid);
	Page<UserEntity> findAll(Specification<UserEntity> spec, Pageable pageable);
}
