package com.sportnetic.repositories.master.entity;

import java.io.Serializable;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sportnetic.repositories.BaseAuditTrailEntity;

import lombok.Data;

@Entity
@Data
@Table(name = "M_ARENA", schema = "SPORT_NETIC_MASTER")
public class ArenaEntity extends BaseAuditTrailEntity implements Serializable {/**
	 * 
	 */
	private static final long serialVersionUID = 76075508399514314L;

	@Id
	@GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
	@GeneratedValue(generator = "UUID")
	@Column(name = "ARENA_ID", nullable = false, unique = true)
	private UUID arenaId;
	
	@Column(name = "ARENA_TYPE_ID", nullable = false, unique = true)
	private UUID arenaTypeId;
	
	@Column(name = "VENUE_ID", nullable = false, unique = true)
	private UUID venuId;
	
	@Column(name = "CATEGORY_ID", nullable = false, unique = true)
	private UUID categoryId;
	
	@Column(name = "NAME" )
	private String name;
	
	@Column(name = "TYPE" )
	private String type;
	
	@Column(name = "CATEGORY" )
	private String category;
	
	
	@Column(name = "ARENA_SPEC" )
	private String arenaSpec;
	
	@Column(name = "PRICE" )
	private Integer price;
	
	@ManyToOne(targetEntity = VenueEntity.class, fetch = FetchType.LAZY)
	@JoinColumn(name = "VENUE_ID", nullable = true, updatable = false, insertable = false)
	@JsonIgnore
	private VenueEntity venue;
}
