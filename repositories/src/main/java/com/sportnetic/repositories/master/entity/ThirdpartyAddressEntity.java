package com.sportnetic.repositories.master.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sportnetic.feign.dto.master.ThirdpartyAddressDto;
import com.sportnetic.repositories.BaseAuditTrailEntity;

import lombok.Data;

@Entity
@Data
@Table(name = "THIRDPARTY_ADDRESS", schema = "SPORT_NETIC_MASTER")
public class ThirdpartyAddressEntity extends BaseAuditTrailEntity implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3724600288887657469L;

	@Id
	@GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
	@GeneratedValue(generator = "UUID")
	@Column(name = "THIRDPARTY_ADDRESS_ID", nullable = false, unique = true)
	private UUID thirdpartyAddressId;

	@Column(name = "THIRDPARTY_ID", nullable = false)
	private UUID thirdpartyId;

	@Column(name = "POSTAL_CODE", nullable = false)
	private String postalCode;

	@Column(name = "DETAIL_ADDRESS", nullable = false)
	private String detailAddress;

	@ManyToOne(targetEntity = VenueEntity.class, fetch = FetchType.LAZY)
	@JoinColumn(name = "THIRDPARTY_ID", nullable = true, updatable = false, insertable = false)
	@JsonIgnore
	private VenueEntity venue;

	public ThirdpartyAddressEntity() {

	}

	public ThirdpartyAddressEntity(ThirdpartyAddressDto thirdPartyAddressDto) {
		if (thirdPartyAddressDto.getThirdpartyAddressId() != null
				&& thirdPartyAddressDto.getThirdpartyAddressId().length() > 0) {
			this.thirdpartyId = UUID.fromString(thirdPartyAddressDto.getThirdpartyAddressId());
		}
		this.thirdpartyId = UUID.fromString(thirdPartyAddressDto.getThirdpartyId());
		this.postalCode = thirdPartyAddressDto.getPostalCode();
		this.detailAddress = thirdPartyAddressDto.getDetailAddress();
		this.isActive = thirdPartyAddressDto.getIsActive();

	}
}
