package com.sportnetic.repositories.master.entity;

import java.io.Serializable;
import java.util.List;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sportnetic.repositories.BaseAuditTrailEntity;

import lombok.Data;

@Entity
@Data
@Table(name = "VENUE_GROUP", schema = "SPORT_NETIC_MASTER")
public class VenueGroupEntity extends BaseAuditTrailEntity implements Serializable {
	/**
	* 
	*/
	private static final long serialVersionUID = 8338844613303234750L;

	@Id
	@GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
	@GeneratedValue(generator = "UUID")
	@Column(name = "VENUE_GROUP_ID", nullable = false, unique = true)
	private UUID venueGroupId;

	@Column(name = "USER_ID", nullable = false)
	private UUID userId;
	
	@Column(name = "name")
	private String name;
	
	@JsonIgnore
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "R_VENUE", schema = "SPORT_NETIC_MASTER", joinColumns = @JoinColumn(name = "VENUE_GROUP_ID", referencedColumnName = "VENUE_GROUP_ID", updatable = false, nullable = true), 
	inverseJoinColumns = @JoinColumn(name = "VENUE_ID", referencedColumnName = "VENUE_ID", updatable = false, nullable = true))
	private List<VenueEntity> venues;

}
