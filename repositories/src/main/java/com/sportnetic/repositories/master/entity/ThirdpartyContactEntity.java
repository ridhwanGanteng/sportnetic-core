package com.sportnetic.repositories.master.entity;

import java.io.Serializable;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sportnetic.feign.dto.master.ThirdpartyContactDto;
import com.sportnetic.repositories.BaseAuditTrailEntity;

import lombok.Data;

@Entity
@Data
@Table(name = "THIRDPARTY_CONTACT", schema = "SPORT_NETIC_MASTER")
public class ThirdpartyContactEntity extends BaseAuditTrailEntity implements Serializable {
	

	/**
		 * 
		 */
	private static final long serialVersionUID = -405485553431409693L;

	@Id
	@GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
	@GeneratedValue(generator = "UUID")
	@Column(name = "THIRDPARTY_CONTACT_ID", nullable = false, unique = true)
	private UUID thirdpartyContactId;

	@Column(name = "THIRDPARTY_ID", nullable = false)
	private UUID thirdPartyId;

	@Column(name = "TYPE")
	private String type;

	@Column(name = "VALUE")
	private String value;

	@ManyToOne(targetEntity = VenueEntity.class, fetch = FetchType.LAZY)
	@JoinColumn(name = "THIRDPARTY_ID", nullable = true, updatable = false, insertable = false)
	@JsonIgnore
	private VenueEntity venue;
	
	public ThirdpartyContactEntity(ThirdpartyContactDto contactDto) {
		if(contactDto.getThirdpartyContactId()!=null && contactDto.getThirdpartyContactId().length()>0) {
			this.thirdpartyContactId=UUID.fromString(contactDto.getThirdpartyContactId());
		}
		
		// TODO Auto-generated constructor stub
	}

}
