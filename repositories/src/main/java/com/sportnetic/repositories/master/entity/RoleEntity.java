package com.sportnetic.repositories.master.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.sportnetic.feign.dto.master.RoleDto;
import com.sportnetic.repositories.BaseAuditTrailEntity;

import lombok.Data;

@Entity
@Data
@Table(name = "M_ROLE", schema = "SPORT_NETIC_MASTER")
public class RoleEntity extends BaseAuditTrailEntity implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 3187868311207946503L;

	@Id
	@GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
	@GeneratedValue(generator = "UUID")
	@Column(name = "ROLE_ID", nullable = false, unique = true)
	private UUID roleId;
	
	@Column(name = "AUTHORITY", nullable = false)
	private String authority;
	
	public RoleEntity(RoleDto roleDto) {
		UUID uuid=null;
		if(roleDto.getRoleId()!=null && roleDto.getRoleId().length()>0) {
			uuid=UUID.fromString(roleDto.getRoleId());
			this.roleId=uuid;
		}
		this.authority=roleDto.getAuthority();
		this.isActive=roleDto.getIsActive();
		this.description=roleDto.getDescription();
		
	}
	
	public RoleDto parseDto() {
		RoleDto roleDto = new RoleDto();
		roleDto.setRoleId(this.roleId.toString());
		roleDto.setAuthority(this.authority);
		roleDto.setCreatedBy(this.createdBy);
		roleDto.setCreatedDate(this.createdDate);
		roleDto.setModifiedBy(this.modifiedBy);
		roleDto.setModifiedDate(this.modifiedDate);
		roleDto.setIsActive(this.isActive);
		roleDto.setDescription(this.description);
		return roleDto;
	}
}
