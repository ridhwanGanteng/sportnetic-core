package com.sportnetic.repositories.master.dao;

import java.util.UUID;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.sportnetic.repositories.master.entity.RoleEntity;
import com.sportnetic.repositories.master.entity.UserEntity;

public interface RoleRepo extends PagingAndSortingRepository<RoleEntity, UUID>{
	Page<RoleEntity> findAll(Specification<RoleEntity> specification, Pageable pageable);

	RoleEntity findByRoleId(UUID uuid);
	
	
}
