package com.sportnetic.repositories.master.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sportnetic.feign.dto.master.MenuGroupDto;
import com.sportnetic.feign.dto.master.RoleDto;
import com.sportnetic.feign.dto.master.UserDto;
import com.sportnetic.repositories.BaseAuditTrailEntity;

import lombok.Data;

@Entity
@Data
@Table(name = "M_USER", schema = "SPORT_NETIC_MASTER")

public class UserEntity extends BaseAuditTrailEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
	@GeneratedValue(generator = "UUID")
	@Column(name = "USER_ID", nullable = false, unique = true)
	private UUID userId;

	@Column(name = "EMAIL", nullable = false)
	private String email;

	@Column(name = "NAME", nullable = false)
	private String nama;

	@Column(name = "BIRTH_DATE")
	private Date birthDate;

	@Column(name = "PASSWORD", nullable = false)
	private String password;

	@Column(name = "GENDER", nullable = false)
	private String gender;
	
	@Column(name = "ADDRESS", nullable = false)
	private String address;

	@Column(name = "PHONE_NUMBER", nullable = false)
	private String phoneNumber;

	@JsonIgnore
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "R_ROLE", schema = "SPORT_NETIC_MASTER", joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "user_id", updatable = false, nullable = true), inverseJoinColumns = @JoinColumn(name = "role_id", referencedColumnName = "role_id", updatable = false, nullable = true))
	private List<RoleEntity> roles;

	@JsonIgnore
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "R_ROLE", schema = "SPORT_NETIC_MASTER", joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "user_id", updatable = false, nullable = true), inverseJoinColumns = @JoinColumn(name = "MENU_GROUP_ID", referencedColumnName = "MENU_GROUP_ID", updatable = false, nullable = true))
	private List<MenuGroupEntity> menuGroups;

	public UserEntity(UserDto userDto) {
		UUID uuid = null;
		if (userDto.getUserId() != null && userDto.getUserId().length() > 0) {
			uuid = UUID.fromString(userDto.getUserId());
			this.userId = uuid;
		}
		this.phoneNumber=userDto.getPhoneNumber();
		this.address=userDto.getAddress();
		this.email = userDto.getEmail();
		this.nama = userDto.getNama();
		this.birthDate = userDto.getBirthDate();
		this.password = userDto.getPassword();
		this.gender = userDto.getGender();
		
	}

	public UserDto parseDto() {
		UserDto userDto = new UserDto();
		userDto.setUserId(this.getUserId().toString());
		userDto.setNama(this.getNama());
		userDto.setBirthDate(this.getBirthDate());
		userDto.setIsActive(this.getIsActive());
		userDto.setCreatedBy(this.getCreatedBy());
		userDto.setModifiedBy(this.getModifiedBy());
		userDto.setModifiedDate(this.getModifiedDate());
		userDto.setCreatedDate(this.getCreatedDate());
		userDto.setGender(this.gender);
		List<RoleDto> roles = new ArrayList<RoleDto>();
		if (this.roles != null && this.roles.size() > 0) {
			for (RoleEntity _role : this.roles) {
				List<MenuGroupDto> menuGroup = new ArrayList<MenuGroupDto>();
				if (this.menuGroups != null && this.menuGroups.size() > 0) {
					roles.add(_role.parseDto());
					for (MenuGroupEntity _menuGroup : this.menuGroups) {
						menuGroup.add(_menuGroup.parseDto());
					}
					roles.get(roles.size() - 1).setMenuGroup(menuGroup);
				}
			}
		}
		userDto.setRoles(roles);
		return userDto;
	}

}
