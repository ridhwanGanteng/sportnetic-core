package com.sportnetic.repositories.master.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sportnetic.feign.dto.master.MenuDto;
import com.sportnetic.feign.dto.master.MenuGroupDto;
import com.sportnetic.repositories.BaseAuditTrailEntity;

import lombok.Data;

@Entity
@Data
@Table(name = "MENU_GROUP", schema = "SPORT_NETIC_MASTER")
public class MenuGroupEntity extends BaseAuditTrailEntity implements Serializable {

	@Id
	@GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
	@GeneratedValue(generator = "UUID")
	@Column(name = "MENU_GROUP_ID", nullable = false, unique = true)
	private UUID menuGroupId;

	@Column(name = "LABEL", nullable = false)
	private String label;

	@Column(name = "ICON", nullable = false)
	private String icon;

	@JsonIgnore
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "R_MENU", schema = "SPORT_NETIC_MASTER", joinColumns = @JoinColumn(name = "MENU_GROUP_ID", referencedColumnName = "MENU_GROUP_ID", updatable = false, nullable = true), inverseJoinColumns = @JoinColumn(name = "MENU_ID", referencedColumnName = "MENU_ID", updatable = false, nullable = true))
	private List<MenuEntity> menus;

	public MenuGroupEntity(MenuGroupDto menuGroupDto) {
		UUID uuid = null;
		if (menuGroupDto.getMenuId() != null && menuGroupDto.getMenuId().length() > 0) {
			uuid = UUID.fromString(menuGroupDto.getMenuId());
			this.menuGroupId = uuid;
		}
		this.icon = menuGroupDto.getIcon();
		this.isActive = menuGroupDto.getIsActive();
		this.description = menuGroupDto.getDescription();
		this.label = menuGroupDto.getLabel();
		if (menuGroupDto.getMenu() != null && menuGroupDto.getMenu().size() > 0) {
			List<MenuEntity> menu = new ArrayList<MenuEntity>();
			for (MenuDto _menu : menuGroupDto.getMenu()) {
				menu.add(new MenuEntity(_menu));
			}
			this.setMenus(menu);
		}
	}

	public MenuGroupDto parseDto() {
		MenuGroupDto menuGroup = new MenuGroupDto();
		menuGroup.setMenuId(this.getMenuGroupId().toString());
		menuGroup.setIcon(this.icon);
		menuGroup.setLabel(this.label);
		menuGroup.setIsActive(this.isActive);
		menuGroup.setDescription(this.description);
		if (this.menus != null && this.menus.size() > 0) {
			List<MenuDto> menu = new ArrayList<MenuDto>();
			for (MenuEntity _menu : this.menus) {
				menu.add(_menu.parseDto());
			}
			menuGroup.setMenu(menu);
		}
		return menuGroup;
	}

}
