package com.sportnetic.repositories.master.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sportnetic.feign.dto.master.MenuDto;
import com.sportnetic.repositories.BaseAuditTrailEntity;

import lombok.Data;

@Entity
@Data
@Table(name = "M_MENU", schema = "SPORT_NETIC_MASTER")
public class MenuEntity extends BaseAuditTrailEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
	@GeneratedValue(generator = "UUID")
	@Column(name = "MENU_ID", nullable = false, unique = true)
	private UUID menuId;

	@Column(name = "LABEL", nullable = false)
	private String label;

	@Column(name = "ICON", nullable = false)
	private String icon;

	@Column(name = "url", nullable = false)
	private String url;

	@JsonIgnore
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "R_MENU", schema = "SPORT_NETIC_MASTER", joinColumns = @JoinColumn(name = "MENU_ID", referencedColumnName = "MENU_ID", updatable = false, nullable = true), inverseJoinColumns = @JoinColumn(name = "MENU_GROUP_ID", referencedColumnName = "MENU_GROUP_ID", updatable = false, nullable = true))
	private List<MenuGroupEntity> menuGroups;

	public MenuEntity(MenuDto menu) {
		UUID uuid = null;
		if (menu.getMenuId() != null && menu.getMenuId().length() > 0) {
			uuid = UUID.fromString(menu.getMenuId());
			this.menuId = uuid;
		}
		this.icon = menu.getIcon();
		this.isActive = menu.getIsActive();
		this.description = menu.getDescription();
		this.label = menu.getLabel();
		this.url = menu.getUrl();
	}

	public MenuDto parseDto() {
		MenuDto menuDto = new MenuDto();
		menuDto.setIcon(this.icon);
		menuDto.setUrl(this.url);
		menuDto.setLabel(this.label);
		menuDto.setIsActive(this.isActive);
		menuDto.setDescription(this.description);
		menuDto.setCreatedBy(this.createdBy);
		menuDto.setCreatedDate(this.createdDate);
		menuDto.setModifiedBy(this.modifiedBy);
		menuDto.setModifiedDate(this.modifiedDate);
		return menuDto;

	}

}
