/*package com.sportnetic.security;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.oauth2.common.DefaultExpiringOAuth2RefreshToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2RefreshToken;
import org.springframework.security.oauth2.common.exceptions.InvalidTokenException;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;

import com.hazelcast.core.HazelcastInstance;

public class JwtHazelcastTokenStore extends JwtTokenStore {	
	
	protected final Logger LOGGER = LogManager.getLogger(this.getClass());
	private HazelcastInstance instance;
	
    public JwtHazelcastTokenStore(JwtAccessTokenConverter jwtTokenEnhancer,HazelcastInstance instance) {    	
		super(jwtTokenEnhancer);
		this.instance = instance;		
	}
	
	
    public JwtHazelcastTokenStore(JwtAccessTokenConverter jwtTokenEnhancer) {
		super(jwtTokenEnhancer);
		// TODO Auto-generated constructor stub
	}


	@Override
	public OAuth2Authentication readAuthentication(OAuth2AccessToken token) {
		LOGGER.debug("readAuthentication from Hazelcast:"+token.toString());		
			if(instance!=null && instance.getLifecycleService().isRunning())
				if(!instance.getMap("security.accessTokenList").containsKey(token.getValue()))
					throw new InvalidTokenException("Access Token Not found");
		return super.readAuthentication(token);				
	}


	@Override
	public OAuth2Authentication readAuthentication(String token) {
		LOGGER.debug("readAuthentication:"+token.toString());
		return super.readAuthentication(token);
	}


	@Override
	public void storeAccessToken(OAuth2AccessToken token, OAuth2Authentication authentication) {
		LOGGER.debug("storeAccessToken:"+token.toString()+" , "+authentication.toString());		
		super.storeAccessToken(token, authentication);
				
		try{
			instance.getMap("security.accessTokenList").putAsync(token.getValue(), authentication.getName(), token.getExpiresIn(), TimeUnit.SECONDS);	
		}catch(Exception e)
		{
			LOGGER.warn("Fail accessing token via Hazelcast",e);
		}
		finally {
			
		}
	}


	@Override
	public OAuth2AccessToken readAccessToken(String tokenValue) {
		LOGGER.debug("readAccessToken:"+tokenValue.toString());
		return super.readAccessToken(tokenValue);
	}


	@Override
	public void removeAccessToken(OAuth2AccessToken token) {
		LOGGER.debug("removeAccessToken:"+token.toString());
		super.removeAccessToken(token);
		
		try{
			instance.getMap("security.accessTokenList").remove(token.getValue());	
		}catch(Exception e)
		{
			LOGGER.warn("Fail accessing token via Hazelcast",e);
		}
		finally {
			
		}
	}


	@Override
	public void storeRefreshToken(OAuth2RefreshToken refreshToken, OAuth2Authentication authentication) {
		LOGGER.debug("storeRefreshToken:"+refreshToken.toString() + " , "+authentication.toString());
		super.storeRefreshToken(refreshToken, authentication);
		
		try{
			if(refreshToken instanceof DefaultExpiringOAuth2RefreshToken)
			{
				DefaultExpiringOAuth2RefreshToken expired = (DefaultExpiringOAuth2RefreshToken) refreshToken;
				Date expDate = expired.getExpiration();
				Date currentDate = new Date();
				int expiredInSeconds = (int)((expDate.getTime()/1000)-(currentDate.getTime()/1000));
				instance.getMap("security.refreshTokenList").putAsync(refreshToken.getValue(), authentication.getName(), expiredInSeconds, TimeUnit.SECONDS);
			}	
		}catch(Exception e)
		{
			LOGGER.warn("Fail accessing token via Hazelcast",e);
		}
		finally {
			
		}					
	}


	@Override
	public OAuth2RefreshToken readRefreshToken(String tokenValue) {
		LOGGER.debug("readRefreshToken:"+tokenValue.toString());
		return super.readRefreshToken(tokenValue);
	}


	@Override
	public OAuth2Authentication readAuthenticationForRefreshToken(OAuth2RefreshToken token) {
		LOGGER.debug("readAuthenticationForRefreshToken from hazelcast:"+token.toString());
				
		if(instance!=null && instance.getLifecycleService().isRunning())
			if(!instance.getMap("security.refreshTokenList").containsKey(token.getValue()))
				throw new InvalidTokenException("Refresh Token Not found");	
		
		return super.readAuthenticationForRefreshToken(token);
	}


	@Override
	public void removeRefreshToken(OAuth2RefreshToken token) {
		LOGGER.debug("removeRefreshToken:"+token.toString());
		super.removeRefreshToken(token);		
		
		try{
			instance.getMap("security.refreshTokenList").remove(token.getValue());
		}catch(Exception e)
		{
			LOGGER.warn("Fail accessing token via Hazelcast",e);
		}
		finally {
			
		}
	}


	@Override
	public void removeAccessTokenUsingRefreshToken(OAuth2RefreshToken refreshToken) {
		LOGGER.debug("removeAccessTokenUsingRefreshToken:"+refreshToken.toString());
		super.removeAccessTokenUsingRefreshToken(refreshToken);
	}


	@Override
	public OAuth2AccessToken getAccessToken(OAuth2Authentication authentication) {
		LOGGER.debug("getAccessToken:"+authentication.toString());
		return super.getAccessToken(authentication);
	}      
}
*/