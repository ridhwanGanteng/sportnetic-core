/**
 * 
 */
package com.sportnetic.constant;

/**
 * @author <a href="mailto:bagus.sugitayasa@sigma.co.id">GusdeGita</a>
 * @version $Id: DomainServiceConstant.java 3006 2018-04-10 10:02:51Z bagus.sugitayasa $ 
 */
public class DomainServiceConstant {
	public static final String MASTER = "telkomsigma-master";
	public static final String SECURITY = "telkomsigma-security";
	public static final String WORKFLOW = "telkomsigma-workflow";
	public static final String REPORT = "telkomsigma-report";
	public static final String UNDERWRITING = "askrindo-underwriting";
	public static final String BUSINESS_PARTNER = "askrindo-business-partner";
}
