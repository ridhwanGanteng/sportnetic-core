package com.sportnetic.constant;

/**
 * DocumentStatusConstant
 * @author <a href="mailto:raka.sanjaya@sigma.co.id">Raka Sanjaya</a>
 * @version $Id: DocumentStatusConstant.java 2745 2018-03-20 08:17:43Z raka.sanjaya $
 */

public class DocumentStatusConstant {	
	public static final Short DRAFT_STATUS = 1;
	public static final Short PENDING_STATUS = 2;
	public static final Short PUBLISH_STATUS = 3;
	public static final Short REJECT_STATUS = 4;
	public static final Short APPROVE_STATUS = 5;
	public static final Short APPROVE_WITH_CONDITION_STATUS = 6;
	public static final Short REVIEW_UDW_CAB_STATUS = 7;
	public static final Short REVIEW_RO_CAB_STATUS = 8;
	public static final Short REVIEW_KASI_CAB_STATUS = 9;
	public static final Short APPROVAL_KABAG_CAB_STATUS = 10;
	public static final Short APPROVAL_PINCAB_STATUS = 11;
	public static final Short APPROVAL_PINWIL_STATUS = 12;
	public static final Short REVIEW_UDW_DIVISI_STATUS = 13;
	public static final Short APPROVAL_KABAG_UDW_DIV_STATUS = 14;
	public static final Short APPROVAL_KADIV_STATUS = 15;
	public static final Short APPROVAL_KOMITE_STATUS = 16;
	public static final Short APPROVAL_DIREKTUR_STATUS = 17;
}
