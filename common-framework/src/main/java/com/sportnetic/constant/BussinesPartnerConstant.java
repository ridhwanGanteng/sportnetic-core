package com.sportnetic.constant;

/**
 * @author <a href="mailto:dino.prasetyo@sigma.co.id">Dino Prasetyo</a>
 * @version $Revision:$
 *          Date: 25/04/18 15.45
 */
public class BussinesPartnerConstant
{
    public static class PartnerType
    {
        public static final String AGENT_ALL = "AGENT";
        public static final String AGENT_INDIVIDU = "AGENT.INDIVIDU";
        public static final String AGENT_PERUSAHAAN = "AGENT.PERUSAHAAN";
        public static final String BROKER_ALL = "BROKER";
        public static final String BROKER_DLM_NEGERI = "BROKER.DLM_NEGERI";
        public static final String BROKER_LUAR_NEGERI = "BROKER.LUAR_NEGERI";
        public static final String COLLECT_AGENT = "COLLECT_AGENT";
        public static final String DEBITUR = "DEBITUR";
        public static final String KOASURADUR = "KOASURADUR";
        public static final String LOST_ADJ = "LOST_ADJ";
        public static final String MARKETING = "MARKETING";
        public static final String NASABAH_ALL = "NASABAH";
        public static final String NASABAH_BANK = "NASABAH.BANK";
        public static final String NASABAH_INDIVIDU = "NASABAH.INDIVIDU";
        public static final String NASABAH_LEASING = "NASABAH.LEASING";
        public static final String NASABAH_PERUS = "NASABAH.PERUS";
        public static final String NASABAH_PERUS_BUMD = "NASABAH.PERUS.BUMD";
        public static final String NASABAH_PERUS_BUMN = "NASABAH.PERUS.BUMN";
        public static final String NASABAH_PERUS_SWASTA = "NASABAH.PERUS.SWASTA";
        public static final String REASURADUR = "REASURADUR";
        public static final String SURVEYOR = "SURVEYOR";
        public static final String VENDOR = "VENDOR";
    }
}
