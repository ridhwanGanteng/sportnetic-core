/**
 * 
 */
package com.sportnetic.exceptions;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.multipart.MultipartException;

/**
 * @author <a href="mailto:bagus.sugitayasa@sigma.co.id">GusdeGita</a>
 * @version $Id: GlobalExceptionHandler.java 499 2017-12-13 08:25:45Z bagus.sugitayasa $ 
 */
@ControllerAdvice
public class GlobalExceptionHandler {

	@ExceptionHandler(MultipartException.class)
    public String handleError1(MultipartException e) {
        return e.getCause().getMessage();
    }
}