package com.sportnetic.exceptions;

public class InternalServerException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8972880939809113925L;

	public InternalServerException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public InternalServerException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public InternalServerException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public InternalServerException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
