package com.sportnetic;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class BaseService {

	protected final Logger LOGGER = LogManager.getLogger(this.getClass());
}
