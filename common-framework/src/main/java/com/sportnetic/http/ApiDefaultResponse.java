/**
 * 
 */
package com.sportnetic.http;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

/**
 * Crud REST ideal nya return {@link ResponseEntity} dengan format JSON yang akan dilempar ke depan
 * Format default jika proses transaksi sukses yang mungkin dapat digunakan adalah</br>
 * contoh : </br>
 * { "status" : "OK", "data" : "[
		{
			"id": "41EE134A-15BB-4578-AA04-BCC813250691",
			"fieldA": 123,
			"fieldB": "B"
		},
		{
			"id": "B15993D7-7EAA-4D5D-9CA6-30E19B7E0FCD",
			"fieldA": 456,
			"fieldB": "B"
		}
	]"
	</br>
	response dapat ditambahkan jika keperluan selain data, misal : status_code, dsb
 * @author <a href="mailto:bagus.sugitayasa@sigma.co.id">GusdeGita</a>
 * @version $Id: ApiDefaultResponse.java 706 2017-12-20 08:51:19Z bagus.sugitayasa $ 
 */
public class ApiDefaultResponse {

	private HttpStatus status;
	private Object data;
	
	public ApiDefaultResponse() {
		this(null);
	}

	public ApiDefaultResponse(Object data) {
		this.data = data;
	}
	
	public ResponseEntity<ApiDefaultResponse> send(HttpStatus status) {
		this.status = status;
		return new ResponseEntity<ApiDefaultResponse>(this, status);
	}

	public ResponseEntity<ApiDefaultResponse> send(HttpHeaders headers, HttpStatus status) {
		this.status = status;
		return new ResponseEntity<ApiDefaultResponse>(this, headers, status);
	}
	
	public HttpStatus getStatus() {
		return status;
	}
	
	public Object getData() {
		return data;
	}
}
