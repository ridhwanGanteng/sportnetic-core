package com.sportnetic.http;

import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;

import com.sportnetic.utils.ErrorCode;
import com.sportnetic.utils.JsonUtils;
    
/**
 * @author Ridla.Fadilah
 * @version $Id: ApiErrorResponse.java 819 2018-01-02 13:45:01Z rudi.sadria $ 
 */
@Component
public class ApiErrorResponse {
	
	@Autowired
	private MessageSource messageSource;

	@Autowired
	private JsonUtils jsonUtils;
	
	public String errorResponse(ErrorCode errorCode, Locale locale) {
		if(locale == null)
			locale = Locale.forLanguageTag("id-ID");
		Map<String, Object> response = new TreeMap<String, Object>();
		response.put("errorCode", errorCode);
		response.put("errorMessage", messageSource.getMessage(errorCode.name(), null, locale));
		return jsonUtils.objToJson(response);
	}
	
	public String getErrorMessage(ErrorCode errorCode,Locale locale)
	{
		return messageSource.getMessage(errorCode.name(), null, locale);
	}
    
}
