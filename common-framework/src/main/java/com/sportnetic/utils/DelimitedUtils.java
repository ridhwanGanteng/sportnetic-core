package com.sportnetic.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class DelimitedUtils {

	public static String toDelimitedString(Map<String,Object> source)
	{
		String retVal = "";
		for(String key : source.keySet())
		{
			String val = "";
			if(source.get(key)!=null)
				val = source.get(key).toString();
			retVal = retVal + key+"="+val+",";
		}
		//remove last ,
		retVal = retVal.substring(0,retVal.length()-1);
		return retVal;
	}
	
	public static String toDelimitedString(Collection source)
	{
		String retVal = "";
		for(Object key : source)
		{
			String val = key.toString();
			retVal = retVal +val+",";
		}
		//remove last ,
		retVal = retVal.substring(0,retVal.length()-1);
		return retVal;
	}
	
	public static String toDelimitedString(Set source)
	{
		String retVal = "";
		if(source!=null)
			for(Object key : source)
			{
				String val = key.toString();
				retVal = retVal +val+",";
			}
		//remove last ,
		retVal = retVal.substring(0,retVal.length()-1);
		return retVal;
	}
	
	public static Map<String,Object> toMap(String source)
	{
		Map<String,Object> retVal = new LinkedHashMap<String,Object>();
		if(source!=null)
			for(String record :source.split("\\,"))
			{
				if(record.indexOf("=")>=0) {
					String data[]=record.split("\\="); 
					retVal.put(data[0], data[1]);
				}
			}		
		return retVal;
	}
	
	public static Set<String> toSet(String source)
	{
		Set<String> retVal = new LinkedHashSet<String>();
		
		if(source!=null)
			for(String record :source.split("\\,"))
			{
					retVal.add(record);
			}		
		return retVal;
	}
	
	public static Collection<String> toCollection(String source)
	{
		List<String> retVal = new ArrayList<String>();
		
		for(String record :source.split("\\,"))
		{
				retVal.add(record);
		}		
		return retVal;
	}
}
