/**
 * 
 *//*
package com.sportnetic.utils;

import java.lang.reflect.Type;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.modelmapper.Conditions;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.stereotype.Component;

*//**
 * @author <a href="mailto:bagus.sugitayasa@sigma.co.id">GusdeGita</a>
 * @version $Id: ObjectMapperUtils.java 3272 2018-04-23 10:31:24Z raka.sanjaya $
 *//*
@Component
public class ObjectMapperUtils {
	private ModelMapper modelMapper;	
	protected final Logger LOGGER = LogManager.getLogger(this.getClass());

	public ObjectMapperUtils() {
		this.modelMapper = new ModelMapper();
		this.modelMapper.getConfiguration().setPropertyCondition(Conditions.isNotNull())
		.setMatchingStrategy(MatchingStrategies.STRICT);
	}

	*//**
	 * Use for Object transform to another Object/Helper/DTO example :
	 * transforms(staff, StaffHelper.class);</br>
	 * 
	 * @param object
	 * @param cls
	 * @return <T> Object
	 *//*
	public <T> Object transforms(Object object, Class<T> cls) {
		return modelMapper.map(object, cls);
	}

	*//**
	 * @param object
	 * @param type
	 * @return {@link Object}
	 *//*
	public Object transforms(Object object, Type type) {
		return modelMapper.map(object, type);
	}
}
*/