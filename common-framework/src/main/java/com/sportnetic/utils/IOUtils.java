/**
 * 
 */
package com.sportnetic.utils;

import java.awt.datatransfer.MimeTypeParseException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.math.BigDecimal;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.nio.channels.OverlappingFileLockException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import javax.activation.MimetypesFileTypeMap;

import org.springframework.stereotype.Component;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author <a href="mailto:bagus.sugitayasa@sigma.co.id">GusdeGita</a>
 * @version $Id: IOUtils.java 1348 2018-01-24 09:39:13Z bagus.sugitayasa $
 */
@Component
public class IOUtils {

	private static String tmpDir = System.getProperty("java.io.tmpdir");
	private static String fileZipName = "telkomsigma_file.zip";

	private FileInputStream fileInputStream;

	/**
	 * Write file to spesific location
	 * 
	 * @param filePath
	 *            (ex: /app/uploads/filename.txt)
	 * @param fileContent
	 * @throws IOException
	 */
	public void writeFile(String filePath, byte[] fileContent) throws IOException {
		/*
		 * File file = new File(path + File.separator + fileName); FileOutputStream fop
		 * = new FileOutputStream(file); fop.write(fileContent); fop.flush();
		 * fop.close();
		 */
		/* Files.write(Paths.get(filePathString), fileContent); */
		/*
		 * Files.copy(file.getInputStream(),
		 * this.rootLocation.resolve(file.getOriginalFilename()));
		 */

		RandomAccessFile stream = new RandomAccessFile(filePath, "rw");
		FileChannel channel = stream.getChannel();

		FileLock lock = null;
		try {
			lock = channel.tryLock();
			ByteBuffer buffer = ByteBuffer.allocate(fileContent.length);
			buffer.put(fileContent);
			buffer.flip();
			channel.write(buffer);
			lock.release();
		} catch (final OverlappingFileLockException e) {
			stream.close();
			channel.close();
		}
		stream.close();
		channel.close();
	}

	/**
	 * @param filePath
	 *            (ex: /app/uploads/filename.txt)
	 * @param filePathZipName
	 *            (ex: /app/tmp/fileName.zip)
	 * @throws IOException
	 */
	public void zipSingleFile(String filePath, String filePathZipName) throws IOException {
		String fullPath = (filePathZipName != null && !filePathZipName.isEmpty()) ? filePathZipName
				: getFilePathString(fileZipName, tmpDir, true);
		FileOutputStream fos = new FileOutputStream(fullPath);
		ZipOutputStream zipOut = new ZipOutputStream(fos);
		File fileToZip = new File(filePath);
		zipFile(fileToZip, zipOut);
		zipOut.close();
		fos.close();
	}

	/**
	 * @param file
	 *            file to be ziping
	 * @param filePathZipName
	 *            full path filename (ex: /app/tmp/single.zip)
	 * @throws IOException
	 */
	public void zipSingleFile(File file, String filePathZipName) throws IOException {
		String fullPath = (filePathZipName != null && !filePathZipName.isEmpty()) ? filePathZipName
				: getFilePathString(fileZipName, tmpDir, true);
		FileOutputStream fos = new FileOutputStream(fullPath);
		ZipOutputStream zipOut = new ZipOutputStream(fos);
		zipFile(file, zipOut);
		zipOut.close();
		fos.close();
	}

	private void zipFile(File file, ZipOutputStream zipOut) throws IOException {
		FileInputStream fis = new FileInputStream(file);
		ZipEntry zipEntry = new ZipEntry(file.getName());
		zipOut.putNextEntry(zipEntry);
		byte[] bytes = new byte[1024];
		int length;
		while ((length = fis.read(bytes)) >= 0) {
			zipOut.write(bytes, 0, length);
		}
		fis.close();
	}

	/**
	 * @param filePaths
	 *            list of full file path (ex: /app/uploads/file1.txt)
	 * @param zipFullPath
	 *            full path zip file (ex: /app/tmp/file1.zip)
	 * @throws IOException
	 */
	public void zipMultiFile(String[] filePaths, String zipFullPath) throws IOException {
		FileOutputStream fos = new FileOutputStream(zipFullPath);
		ZipOutputStream zipOut = new ZipOutputStream(fos);
		for (String srcFile : filePaths) {
			File fileToZip = new File(srcFile);
			zipFile(fileToZip, zipOut);
		}
		zipOut.close();
		fos.close();
	}

	/**
	 * @param files
	 *            list of {@link File} to be zipping
	 * @param zipFullPath
	 *            full path zip file (ex: /app/tmp/file1.zip)
	 * @throws IOException
	 */
	public void zipMultiFile(List<File> files, String zipFullPath) throws IOException {
		FileOutputStream fos = new FileOutputStream(zipFullPath);
		ZipOutputStream zipOut = new ZipOutputStream(fos);
		for (File srcFile : files) {
			zipFile(srcFile, zipOut);
		}
		zipOut.close();
		fos.close();
	}

	/**
	 * @param fullPathZipFile
	 *            full path file.zip to be unzip (ex: /app/temp/fileUnzip.zip)
	 * @param fullPathUnzipFile
	 *            full path unzip dir (ex: /app/uploads/fileUnzip.txt)
	 * @throws IOException
	 */
	public void unzipFile(String fullPathZipFile, String fullPathUnzipFile) throws IOException {
		byte[] buffer = new byte[1024];
		ZipInputStream zis = new ZipInputStream(new FileInputStream(fullPathZipFile));
		ZipEntry zipEntry = zis.getNextEntry();
		while (zipEntry != null) {
			String fileName = zipEntry.getName();
			File newFile = new File(fullPathUnzipFile + fileName);
			FileOutputStream fos = new FileOutputStream(newFile);
			int len;
			while ((len = zis.read(buffer)) > 0) {
				fos.write(buffer, 0, len);
			}
			fos.close();
			zipEntry = zis.getNextEntry();
		}
		zis.closeEntry();
		zis.close();
	}

	/**
	 * @param filePath
	 *            file full path (ex: /app/uploads/file1.txt)
	 * @param fullPathZipFile
	 *            full path zip file (ex: /app/tmp/file1.zip)
	 * @return byte[] of zip file
	 * @throws IOException
	 */
	public byte[] getAllBytesFromSingleZipFile(String filePath, String fullPathZipFile) throws IOException {
		String fullPath = (fullPathZipFile != null && !fullPathZipFile.isEmpty()) ? fullPathZipFile
				: getFilePathString(fileZipName, tmpDir, true);

		File fileToZip = new File(filePath);
		FileOutputStream fos = new FileOutputStream(fullPath);
		ZipOutputStream zipOut = new ZipOutputStream(fos);

		zipFile(fileToZip, zipOut);
		zipOut.close();
		fos.close();
		byte[] zipEncode = Files.readAllBytes(Paths.get(fullPath));
		FileSystemUtils.deleteRecursively(Paths.get(fullPath).toFile());
		return zipEncode;
	}

	/**
	 * @param file
	 *            file to be zipping
	 * @param fullPathZipFile
	 *            full path zip file (ex: /app/tmp/file1.zip)
	 * @return byte[] of zip file
	 * @throws IOException
	 */
	public byte[] getAllBytesFromSingleZipFile(File file, String fullPathZipFile) throws IOException {
		String fullPath = (fullPathZipFile != null && !fullPathZipFile.isEmpty()) ? fullPathZipFile
				: getFilePathString(fileZipName, tmpDir, true);
		FileOutputStream fos = new FileOutputStream(fullPath);
		ZipOutputStream zipOut = new ZipOutputStream(fos);
		if (file.exists()) {
			zipFile(file, zipOut);
		}
		zipOut.close();
		fos.close();
		byte[] zipEncode = Files.readAllBytes(Paths.get(fullPath));
		FileSystemUtils.deleteRecursively(Paths.get(fullPath).toFile());
		return zipEncode;
	}

	/**
	 * @param filePaths
	 * @param fullPathZipFile
	 * @return byte[] of multiple zip file (ex: /app/tmp/file1.zip)
	 * @throws IOException
	 */
	public byte[] getAllBytesFromMultiZipFile(String[] filePaths, String fullPathZipFile) throws IOException {
		String fullPath = (fullPathZipFile != null && !fullPathZipFile.isEmpty()) ? fullPathZipFile
				: getFilePathString(fileZipName, tmpDir, true);

		FileOutputStream fos = new FileOutputStream(fullPath);
		ZipOutputStream zipOut = new ZipOutputStream(fos);
		for (String filePath : filePaths) {
			File file = new File(filePath);
			zipFile(file, zipOut);
		}
		zipOut.close();
		fos.close();
		byte[] zipEncode = Files.readAllBytes(Paths.get(fullPath));
		FileSystemUtils.deleteRecursively(Paths.get(fullPath).toFile());
		return zipEncode;
	}

	/**
	 * @param files
	 *            files to be zip
	 * @param fullPathZipFile
	 *            full path zip file (ex: /app/tmp/file1.zip)
	 * @return byte[] of multiple zip file
	 * @throws IOException
	 */
	public byte[] getAllBytesFromMultiZipFile(List<File> files, String fullPathZipFile) throws IOException {
		String fullPath = (fullPathZipFile != null && !fullPathZipFile.isEmpty()) ? fullPathZipFile
				: getFilePathString(fileZipName, tmpDir, true);
		FileOutputStream fos = new FileOutputStream(fullPath);
		ZipOutputStream zipOut = new ZipOutputStream(fos);
		for (File srcFile : files) {
			if (srcFile.exists()) {
				zipFile(srcFile, zipOut);
			}
		}
		zipOut.close();
		fos.close();
		byte[] zipEncode = Files.readAllBytes(Paths.get(fullPath));
		FileSystemUtils.deleteRecursively(Paths.get(fullPath).toFile());
		return zipEncode;
	}

	/**
	 * @param file
	 *            {@link MultipartFile} file to be get size
	 * @param sizeUnit
	 *            point to {@link FileSizeUnit}
	 * @return
	 */
	public BigDecimal getSize(MultipartFile file, FileSizeUnit sizeUnit) {
		long bytes = file.getSize();
		double kilobytes = (bytes / 1024);
		double megabytes = (kilobytes / 1024);
		double gigabytes = (megabytes / 1024);
		double terabytes = (gigabytes / 1024);
		double petabytes = (terabytes / 1024);
		double exabytes = (petabytes / 1024);
		double zettabytes = (exabytes / 1024);
		double yottabytes = (zettabytes / 1024);

		BigDecimal size = BigDecimal.ZERO;
		if (sizeUnit.equals(FileSizeUnit.bytes)) {
			size = BigDecimal.valueOf(bytes);
		} else if (sizeUnit.equals(FileSizeUnit.kilobytes)) {
			size = BigDecimal.valueOf(kilobytes);
		} else if (sizeUnit.equals(FileSizeUnit.megabytes)) {
			size = BigDecimal.valueOf(megabytes);
		} else if (sizeUnit.equals(FileSizeUnit.gigabytes)) {
			size = BigDecimal.valueOf(gigabytes);
		} else if (sizeUnit.equals(FileSizeUnit.terabytes)) {
			size = BigDecimal.valueOf(terabytes);
		} else if (sizeUnit.equals(FileSizeUnit.petabytes)) {
			size = BigDecimal.valueOf(petabytes);
		} else if (sizeUnit.equals(FileSizeUnit.exabytes)) {
			size = BigDecimal.valueOf(exabytes);
		} else if (sizeUnit.equals(FileSizeUnit.zettabytes)) {
			size = BigDecimal.valueOf(zettabytes);
		} else if (sizeUnit.equals(FileSizeUnit.yottabytes)) {
			size = BigDecimal.valueOf(yottabytes);
		}

		return size;
	}

	/**
	 * @param file
	 *            {@link File} to be get size
	 * @param sizeUnit
	 *            point to {@link FileSizeUnit}
	 * @return
	 */
	public BigDecimal getSize(File file, FileSizeUnit sizeUnit) {
		long bytes = file.length();
		double kilobytes = (bytes / 1024);
		double megabytes = (kilobytes / 1024);
		double gigabytes = (megabytes / 1024);
		double terabytes = (gigabytes / 1024);
		double petabytes = (terabytes / 1024);
		double exabytes = (petabytes / 1024);
		double zettabytes = (exabytes / 1024);
		double yottabytes = (zettabytes / 1024);

		BigDecimal size = BigDecimal.ZERO;
		if (sizeUnit.equals(FileSizeUnit.bytes)) {
			size = BigDecimal.valueOf(bytes);
		} else if (sizeUnit.equals(FileSizeUnit.kilobytes)) {
			size = BigDecimal.valueOf(kilobytes);
		} else if (sizeUnit.equals(FileSizeUnit.megabytes)) {
			size = BigDecimal.valueOf(megabytes);
		} else if (sizeUnit.equals(FileSizeUnit.gigabytes)) {
			size = BigDecimal.valueOf(gigabytes);
		} else if (sizeUnit.equals(FileSizeUnit.terabytes)) {
			size = BigDecimal.valueOf(terabytes);
		} else if (sizeUnit.equals(FileSizeUnit.petabytes)) {
			size = BigDecimal.valueOf(petabytes);
		} else if (sizeUnit.equals(FileSizeUnit.exabytes)) {
			size = BigDecimal.valueOf(exabytes);
		} else if (sizeUnit.equals(FileSizeUnit.zettabytes)) {
			size = BigDecimal.valueOf(zettabytes);
		} else if (sizeUnit.equals(FileSizeUnit.yottabytes)) {
			size = BigDecimal.valueOf(yottabytes);
		}

		return size;
	}

	/**
	 * @param p_fileName
	 *            fileName of file
	 * @return String of Content-Type of file by fileName
	 * @throws MimeTypeParseException
	 */
	public String getContentTypeFromSource(String p_fileName) throws MimeTypeParseException {
		return p_fileName == null ? "application/octet-stream" : new MimetypesFileTypeMap().getContentType(p_fileName);
	}

	/**
	 * @param fullPath
	 *            of file
	 * @return MD5 Hashing
	 * @throws NoSuchAlgorithmException
	 * @throws IOException
	 */
	public String HashingMD5(String fullPath) throws NoSuchAlgorithmException, IOException {
		return Hashing("MD5", fullPath);
	}

	/**
	 * @param fullPath
	 *            of file
	 * @return SHA-256 Hashing
	 * @throws NoSuchAlgorithmException
	 * @throws IOException
	 */
	public String HashingSHA256(String fullPath) throws NoSuchAlgorithmException, IOException {
		return Hashing("SHA-256", fullPath);
	}

	private String Hashing(String hashingType, String fullPath) throws NoSuchAlgorithmException, IOException {
		MessageDigest messageDigest = MessageDigest.getInstance(hashingType);
		fileInputStream = new FileInputStream(fullPath);

		byte[] dataBytes = new byte[1024];

		int nread = 0;
		while ((nread = fileInputStream.read(dataBytes)) != -1) {
			messageDigest.update(dataBytes, 0, nread);
		}

		byte[] mdbytes = messageDigest.digest();

		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < mdbytes.length; i++) {
			sb.append(Integer.toString((mdbytes[i] & 0xff) + 0x100, 16).substring(1));
		}

		return sb.toString();
	}

	/**
	 * @param filename
	 * @param fileLocation
	 * @param appendCurrentTimeMillis
	 *            true for append currentTimeMillis into full path
	 * @return full path, if appendCurrentTimeMillis true = full path with
	 *         currentTimeMillis, else otherwise
	 */
	public String getFilePathString(String filename, String fileLocation, boolean appendCurrentTimeMillis) {
		StringBuilder sb = new StringBuilder();
		sb.append(fileLocation);
		sb.append(File.separator);
		if (appendCurrentTimeMillis) {
			sb.append(System.currentTimeMillis());
		}
		sb.append(filename);
		return sb.toString();
	}

	/**
	 * @param fileName
	 * @return extension of file
	 */
	public String getExtension(String fileName) {
		String fileExtension = fileName.substring(fileName.lastIndexOf(".") + 1, fileName.length());
		return fileExtension;
	}

	/**
	 * @param fileName
	 * @return filename without extension
	 */
	public String getFilenameWithoutExtension(String fileName) {
		String fn = fileName.substring(0, fileName.lastIndexOf("."));
		return fn;
	}

	/**
	 * @param fileName
	 * @param extension
	 * @return boolean true if contain extension, false otherwise
	 */
	public boolean extension(String fileName, String extension) {
		boolean status = false;

		if (extension != null && extension.length() > 0) {
			String ext[] = extension.split(",");
			String fileExtension = getExtension(fileName);

			for (int i = 0; i < ext.length; i++) {
				String extIsTrimable = ext[i].trim();
				if ((fileExtension).equals(extIsTrimable)) {
					status = true;
				}
			}
		}

		return status;
	}

	/**
	 * @param fileName
	 * @param prefix
	 * @return boolean true if filename contain prefix, false otherwise
	 */
	public boolean prefix(String fileName, String prefix) {
		boolean status = false;
		if (prefix != null) {
			if (fileName.startsWith(prefix)) {
				status = true;
			}
		}

		return status;
	}

	/**
	 * @param fullPathFiles
	 *            array of full path string to be delete
	 */
	public void deleteFileOnDisk(String[] fullPathFiles) throws IOException {
		for (String path : fullPathFiles) {
			File file2Delete = new File(path);
			FileSystemUtils.deleteRecursively(file2Delete);
		}
	}

	/**
	 * @param files
	 *            list of file to be delete
	 */
	public void deleteFileOnDisk(List<File> files) throws IOException {
		for (File file : files) {
			FileSystemUtils.deleteRecursively(file);
		}
	}

	/**
	 * @param fullPathFiles full path string to be delete
	 */
	public void deleteFileOnDisk(String fullPathFile) throws IOException {
		File file2Delete = new File(fullPathFile);
		FileSystemUtils.deleteRecursively(file2Delete);
	}

	/**
	 * @param files file to be delete
	 */
	public void deleteFileOnDisk(File file) throws IOException {
		FileSystemUtils.deleteRecursively(file);
	}
}
