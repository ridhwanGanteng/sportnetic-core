/*package com.sportnetic.utils;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.BooleanUtils;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;

public class UserUtils {

	public static String getUUID(OAuth2Authentication auth)
	{
		Object details = auth.getDetails();        
		if (auth!=null && details!=null && details instanceof OAuth2AuthenticationDetails ){
			OAuth2AuthenticationDetails oAuth2AuthenticationDetails = (OAuth2AuthenticationDetails)details;
			return getDecodedDetails(oAuth2AuthenticationDetails,"uuid");
		}  
		return null;
	}
	
	public static String getBranchId(OAuth2Authentication auth)
	{
		Object details = auth.getDetails();        
		if (auth!=null && details!=null && details instanceof OAuth2AuthenticationDetails ){
			OAuth2AuthenticationDetails oAuth2AuthenticationDetails = (OAuth2AuthenticationDetails)details;
			return getDecodedDetails(oAuth2AuthenticationDetails,"branchId");
		}  
		return null;
	}
	
	public static Boolean isHo(OAuth2Authentication auth)
	{
		Object details = auth.getDetails();        
		if (auth!=null && details!=null && details instanceof OAuth2AuthenticationDetails ){
			OAuth2AuthenticationDetails oAuth2AuthenticationDetails = (OAuth2AuthenticationDetails)details;
			String booleanStr = getDecodedDetails(oAuth2AuthenticationDetails,"isHo");
			return BooleanUtils.toBooleanObject(booleanStr);
		}  
		return null;
	}
	
	public static List getProductAuths(OAuth2Authentication auth)
	{
		Object details = auth.getDetails();        
		if (auth!=null && details!=null && details instanceof OAuth2AuthenticationDetails ){
			OAuth2AuthenticationDetails oAuth2AuthenticationDetails = (OAuth2AuthenticationDetails)details;
			return getDecodedDetails(oAuth2AuthenticationDetails,"productAuths");
		}  
		return null;
	}
	
	public static List<String> getRoleList(OAuth2Authentication auth)
	{
		Object details = auth.getDetails();        
		if (auth!=null && details!=null && details instanceof OAuth2AuthenticationDetails ){
			OAuth2AuthenticationDetails oAuth2AuthenticationDetails = (OAuth2AuthenticationDetails)details;
			return getDecodedDetails(oAuth2AuthenticationDetails,"roleList");
		}  
		return null;
	}
	
	public static List<String> getRoleNameList(OAuth2Authentication auth)
	{
		Object details = auth.getDetails();        
		if (auth!=null && details!=null && details instanceof OAuth2AuthenticationDetails ){
			OAuth2AuthenticationDetails oAuth2AuthenticationDetails = (OAuth2AuthenticationDetails)details;
			return getDecodedDetails(oAuth2AuthenticationDetails,"authorities");
		}  
		return null;
	}
	
	
	private static <T> T getDecodedDetails(OAuth2AuthenticationDetails oAuth2AuthenticationDetails,String key)
	{
	    Map<String, Object> decodedDetails = (Map<String, Object>)oAuth2AuthenticationDetails.getDecodedDetails();
	    if(decodedDetails.get(key)!=null)
	    	return (T)decodedDetails.get(key);
	    return null;
	}
	
	public static String getAccessToken(OAuth2Authentication auth)
	{
		Object details = auth.getDetails();        
		if (auth!=null && details!=null && details instanceof OAuth2AuthenticationDetails ){
			OAuth2AuthenticationDetails oAuth2AuthenticationDetails = (OAuth2AuthenticationDetails)details;
			return oAuth2AuthenticationDetails.getTokenValue();
		}  
		return null;
	}
}
*/