/**
 * 
 */
package com.sportnetic.utils;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.type.TypeFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Common mapper utils to convert: </br>
 * <li>Json to List of Object
 * <li>Json to Map of Object
 * <li>Object to String of Json
 * <li>and Casting Collection </br>
 * 
 * @author <a href="mailto:bagus.sugitayasa@sigma.co.id">GusdeGita</a>
 * @version $Id: JsonUtils.java 2760 2018-03-21 03:31:11Z agung_putra $
 */
@Component
public class JsonUtils {

	ObjectMapper objectMapper;
	ObjectWriter objectWriter;

	@Autowired
	public void setObjectMapper( ObjectMapper objectMapper ) {
		this.objectMapper = objectMapper;
	}
	
	protected final Logger LOGGER = LogManager.getLogger(this.getClass());

//	public JsonUtils() {
//		objectMapper = new ObjectMapper();
//		objectWriter = new ObjectMapper().writer().withDefaultPrettyPrinter();
//		this.objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
//		this.objectMapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
//	}

	/**
	 * Convert JSON formatted String Into Java POJO obj
	 * 
	 * @param json
	 *            Source string
	 * @param cls
	 *            Class type of returned obj
	 * @return Object Convert result from JSON formatted String to POJO obj
	 * @throws IOException
	 * @throws JsonParseException
	 *
	 */
	public <T> Object fromJson(String json, Class<T> cls) throws IOException, JsonParseException {
		return objectMapper.readValue(json, cls);
	}

	public <T> Object fromMap(Map<String, Object> map, Class<T> cls) throws JsonParseException, IOException {
		return this.objectMapper.convertValue(map, cls);
	}

	public <T> Object fromListMap(List<Map<String, Object>> list,
			@SuppressWarnings("rawtypes") TypeReference typeReference) throws JsonParseException, IOException {
		return this.objectMapper.convertValue(list, typeReference);
	}

	public <T> List<T> jsonToListOfObj(Class<?> typeKey, String json) throws Exception {
		List<T> listo = null;
		try {
			listo = objectMapper.readValue(json,
					TypeFactory.defaultInstance().constructCollectionType(List.class, typeKey));
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
		}
		return listo;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Map<String, Object> jsonToMapOfObject(String json) throws Exception {
		Map<String, Object> mapto = new HashMap();
		try {
			mapto = objectMapper.readValue(json, new TypeReference<Map<String, Object>>() {
			});
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
		}
		return mapto;
	}

	/**
	 * Convert an POJO obj into JSON formatted String
	 * 
	 * @param obj
	 * @return {@link String} of json
	 */
	public String objToJson(Object obj) {
		String json = "";
		try {
			json = objectMapper.writeValueAsString(obj);
		} catch (JsonProcessingException e) {
			LOGGER.error(e.getMessage());
		}
		return json;
	}

	@SuppressWarnings("rawtypes")
	public <T> List<T> castCollection(List srcList, Class<T> clas) {
		List<T> list = new ArrayList<T>();
		for (Object obj : srcList) {
			if (obj != null && clas.isAssignableFrom(obj.getClass()))
				list.add(clas.cast(obj));
		}
		return list;
	}

    public Map objToMap(Object obj) {
        Map map = new HashMap<String, Object>();
        map = objectMapper.convertValue(obj, Map.class);
        return map;
    }

}
