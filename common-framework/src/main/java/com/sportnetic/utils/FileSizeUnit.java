/**
 * 
 */
package com.sportnetic.utils;

/**
 * @author <a href="mailto:bagus.sugitayasa@sigma.co.id">GusdeGita</a>
 * @version $Id: FileSizeUnit.java 1293 2018-01-23 03:53:44Z bagus.sugitayasa $ 
 */
public enum FileSizeUnit {

	bytes("bytes"),
	kilobytes("kilobytes"),
	megabytes("megabytes"),
	gigabytes("gigabytes"),
	terabytes("terabytes"),
	petabytes("petabytes"),
	exabytes("exabytes"),
	zettabytes("zettabytes"),
	yottabytes("yottabytes");
	
	private String sizeUnit;
	
	FileSizeUnit(String sizeUnit) {
		this.sizeUnit = sizeUnit;
	}

	public String getSizeUnit() {
		return sizeUnit;
	}
}
