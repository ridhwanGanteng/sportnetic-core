package com.sportnetic.configuration;

import javax.servlet.Filter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.github.isrsal.logging.LoggingFilter;

@Configuration
public class RestLogFilter {	
	protected final Logger LOGGER = LogManager.getLogger(this.getClass());
	
	@Bean
	public FilterRegistrationBean logFilterRegistration() {
		LOGGER.info("Seting Rest Log Filter");
		FilterRegistrationBean filterRegBean = new FilterRegistrationBean();
		filterRegBean.setFilter(new LoggingFilter());
		filterRegBean.addUrlPatterns("/*");
		filterRegBean.setEnabled(true);
		return filterRegBean;
	}

	@Bean
	public Filter logFilter() {
		return new LoggingFilter();
	}
    
}
