/*package com.sportnetic.configuration;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.CacheManager;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
//import org.springframework.session.hazelcast.config.annotation.web.http.EnableHazelcastHttpSession;

import com.hazelcast.client.HazelcastClient;
import com.hazelcast.client.config.ClientConfig;
import com.hazelcast.client.config.ClientConnectionStrategyConfig.ReconnectMode;
import com.hazelcast.config.NetworkConfig;
import com.hazelcast.client.config.XmlClientConfigBuilder;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.spring.cache.HazelcastCacheManager;

@Configuration
public class HazelCastDistributedSessionConfig {
	protected final Log LOGGER = LogFactory.getLog(getClass()); 
	
	@Bean
	public HazelcastInstance hazelcastInstance() {
		LOGGER.info("Initialize Hazelcast Distributed Client Config");
		HazelcastInstance instance = null;
		
		
		
			try {
//				log.info("Getting XML configuration from config server");
//				ClientConfig hazelcastConfig = new XmlClientConfigBuilder(configServerHost+"/app/default/trunk/hazelcast-client.xml").build();
//				log.info("Successfully get XML configuration from config server");
				ClientConfig hazelcastConfig = new XmlClientConfigBuilder("classpath:hazelcast-client.xml").build();
				
//				NetworkConfig networkConfig = new NetworkConfig();
//				
//				hazelcastConfig.setNetworkConfig(networkConfig);
				
				hazelcastConfig.getConnectionStrategyConfig()
					.setAsyncStart(true)
					.setReconnectMode(ReconnectMode.ASYNC);				
				hazelcastConfig.setConnectionAttemptLimit(Integer.MAX_VALUE);
				hazelcastConfig.setConnectionAttemptPeriod(5000);
				LOGGER.info("Successfully get XML configuration from config server");
				LOGGER.info("Trying to connect to Hazelcast Server");
				instance = HazelcastClient.newHazelcastClient(hazelcastConfig);
				LOGGER.info("Successfully connected to hazelcast server");
			} catch (Exception e) {
				LOGGER.error("Failed connect to Hazelcast Server",e);
			}
		return instance;
	}

	@Bean
	public CacheManager cacheManager() {
		LOGGER.info("Registering Hazelcast Cache Manager");
		HazelcastInstance instance = hazelcastInstance();
		CacheManager manager = new HazelcastCacheManager(instance);
		return manager;
	}
}
*/