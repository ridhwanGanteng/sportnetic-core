package com.sportnetic.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;

/**
 * Spring EL Config
 * 
 * @author <a href="mailto:raka.sanjaya@sigma.co.id">Raka Sanjaya</a>
 * @version $Id: SpelConfiguration.java 2614 2018-03-14 01:56:05Z raka.sanjaya $
 */

@Configuration
public class SpelConfiguration {

	@Bean
	public ExpressionParser getExpressionParser() {
		return new SpelExpressionParser();
	}
	
	@Bean
	public StandardEvaluationContext getEvalutionContext() {
		return new StandardEvaluationContext();
	}
}
