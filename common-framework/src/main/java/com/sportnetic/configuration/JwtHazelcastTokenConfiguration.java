/*package com.sportnetic.configuration;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;

import com.hazelcast.core.HazelcastInstance;
import com.sportnetic.security.JwtHazelcastTokenStore;

@Configuration
public class JwtHazelcastTokenConfiguration {

    @Autowired
	private HazelcastInstance hazelcastInstance;
	
    @Value("${security.oauth2.resource.jwt.key-value}")
    private String jwtKey;
    
    @Bean
    public JwtAccessTokenConverter jwtAccessTokenConverter() {
        final JwtAccessTokenConverter jwtAccessTokenConverter = new JwtAccessTokenConverter() {
			@Override
			public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
				Map<String, Object> temp = new LinkedHashMap<String, Object>(accessToken.getAdditionalInformation());
				accessToken.getAdditionalInformation().clear();
				accessToken.getAdditionalInformation().put("createdDatetime", new Date().getTime());
				accessToken.getAdditionalInformation().put("device", temp.get("device"));
				accessToken.getAdditionalInformation().put("ipAddress", temp.get("ipAddress"));
				accessToken.getAdditionalInformation().put("uuid", temp.get("uuid"));
				accessToken.getAdditionalInformation().put("branchId", temp.get("branchId"));
				accessToken.getAdditionalInformation().put("productAuths", temp.get("productAuths"));
				accessToken.getAdditionalInformation().put("roleList", temp.get("roleList"));
				accessToken.getAdditionalInformation().put("isHo", temp.get("isHo"));
				
				OAuth2AccessToken newAccessToken = super.enhance(accessToken, authentication);
				newAccessToken.getAdditionalInformation().putAll(temp);
				return newAccessToken;
			}

			@Override
			public OAuth2Authentication extractAuthentication(Map<String, ?> map) {
				OAuth2Authentication outh = super.extractAuthentication(map);
				outh.setDetails(map);
				return outh;
			}        
			
			
        };
        jwtAccessTokenConverter.setSigningKey(jwtKey);
        return jwtAccessTokenConverter;
    }

    @Bean
    public TokenStore tokenStore() {
        return new JwtHazelcastTokenStore(jwtAccessTokenConverter(),hazelcastInstance);
    }        
}
*/