/*package com.sportnetic.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.client.OAuth2ClientContext;



@Configuration
public class FeignConfiguration {

	@Bean
	public RequestInterceptor oauth2BatchRequestInterceptor(OAuth2ClientContext oauth2ClientContext) {
		return new RequestInterceptor() {
			
			@Override
			public void apply(RequestTemplate template) {
				if(oauth2ClientContext.getAccessToken()!=null)
				{
					template.header("Authorization",
					String.format("%s %s",
					oauth2ClientContext.getAccessToken().getTokenType(),
					oauth2ClientContext.getAccessToken().getValue()));
				}
			}
		};		
	}

	@Bean
    public Client client() throws NoSuchAlgorithmException, KeyManagementException {
    	TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
			public java.security.cert.X509Certificate[] getAcceptedIssuers() {
				return null;
			}

			@Override
			public void checkClientTrusted(java.security.cert.X509Certificate[] arg0, String arg1)
					throws CertificateException {
			}

			@Override
			public void checkServerTrusted(java.security.cert.X509Certificate[] arg0, String arg1)
					throws CertificateException {
			}
		} };

		SSLContext sc = null;
		try {
			sc = SSLContext.getInstance("SSL");
		} catch (NoSuchAlgorithmException e1) {
			System.err.println("SSL Instance Fetching Failed, " + e1.getMessage());
		}
		try {
			sc.init(null, trustAllCerts, null);
		} catch (KeyManagementException e) {
			System.err.println("SSL Context Initializing Failed, " + e.getMessage());
		}

        Client trustSSLSockets = new Client.Default(sc.getSocketFactory(), new HostnameVerifier() {
            @Override
            public boolean verify(String hostname, SSLSession session) {
            	return true;
            }
        });
        return trustSSLSockets;
    }

}
*/