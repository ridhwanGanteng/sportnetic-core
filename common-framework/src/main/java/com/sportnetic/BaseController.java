package com.sportnetic;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.sportnetic.exceptions.DataNotFoundException;
import com.sportnetic.exceptions.InternalServerException;
import com.sportnetic.exceptions.SystemErrorException;
import com.sportnetic.http.ApiErrorResponse;
import com.sportnetic.utils.ErrorCode;

public class BaseController {
	protected final Logger LOGGER = LogManager.getLogger(this.getClass());
	
	@Autowired
	protected ApiErrorResponse errorResponse;

	/**
	 * Handles all Exceptions not addressed by more specific
	 * <code>@ExceptionHandler</code> methods. Creates a response with the Exception
	 * Attributes in the response body as JSON and a HTTP status code of 500,
	 * internal server error.
	 *
	 * @param exception
	 *            An Exception instance.
	 * @param request
	 *            The HttpServletRequest in which the Exception was raised.
	 * @return A ResponseEntity containing the Exception Attributes in the body and
	 *         a HTTP status code 500.
	 */	
	@ExceptionHandler(Exception.class)
	public ResponseEntity<BaseResponse> handleException(HttpServletRequest request, Exception exception) {
		exception.printStackTrace();
		LOGGER.error("Handling Exception", exception.getCause());
		
		Locale locale = null;
		String acceptLanguage = request.getHeader("Accept-Language");
		if(acceptLanguage != null)
			locale = new Locale(acceptLanguage);
		Map<String, String> respStatusMessage = new HashMap<String, String>();
		respStatusMessage.put(ErrorCode.ERR_SYS0500.name(), errorResponse.errorResponse(ErrorCode.ERR_SYS0500, locale));
		BaseResponse baseResponse = new BaseResponse();
		baseResponse.setRespStatusData("failure");
		baseResponse.setRespStatusMessage(respStatusMessage);
		baseResponse.setError(respStatusMessage.get(ErrorCode.ERR_SYS0500.name()));
		return new ResponseEntity<BaseResponse>(baseResponse,
				ErrorCode.ERR_SYS0500.getStatus());
	}

	/**
	 * Handles JPA NoResultExceptions thrown from web service controller methods.
	 * Creates a response with Exception Attributes as JSON and HTTP status code
	 * 404, not found.
	 *
	 * @param noResultException
	 *            A NoResultException instance.
	 * @param request
	 *            The HttpServletRequest in which the NoResultException was raised.
	 * @return A ResponseEntity containing the Exception Attributes in the body and
	 *         HTTP status code 404.
	 */
	@ExceptionHandler(DataNotFoundException.class)
	public ResponseEntity<BaseResponse> handleNoResultException(DataNotFoundException exception,
			HttpServletRequest request) {
		LOGGER.error(stackTrace(exception));
		
		Locale locale = null;
		String acceptLanguage = request.getHeader("Accept-Language");
		if(acceptLanguage != null)
			locale = new Locale(acceptLanguage);
		Map<String, String> respStatusMessage = new HashMap<String, String>();
		respStatusMessage.put(ErrorCode.ERR_SYS0001.name(), errorResponse.errorResponse(ErrorCode.ERR_SYS0001, locale));
		BaseResponse baseResponse = new BaseResponse();
		baseResponse.setRespStatusData("failure");
		baseResponse.setRespStatusMessage(respStatusMessage);
		baseResponse.setError(respStatusMessage.get(ErrorCode.ERR_SYS0001.name()));
		return new ResponseEntity<BaseResponse>(baseResponse,
				ErrorCode.ERR_SYS0001.getStatus());
	}
	
	@ExceptionHandler(InternalServerException.class)
	public ResponseEntity<BaseResponse> handleInternalException(HttpServletRequest request, InternalServerException exception) {
		exception.printStackTrace();
		LOGGER.error(stackTrace(exception));
		
		Locale locale = null;
		String acceptLanguage = request.getHeader("Accept-Language");
		if(acceptLanguage != null)
			locale = new Locale(acceptLanguage);
		Map<String, String> respStatusMessage = new HashMap<String, String>();
		respStatusMessage.put(ErrorCode.ERR_SYS0500.name(), errorResponse.errorResponse(ErrorCode.ERR_SYS0500, locale));
		BaseResponse baseResponse = new BaseResponse();
		baseResponse.setRespStatusData("failure");
		baseResponse.setRespStatusMessage(respStatusMessage);
		baseResponse.setError(respStatusMessage.get(ErrorCode.ERR_SYS0500.name()));
		return new ResponseEntity<BaseResponse>(baseResponse,
				ErrorCode.ERR_SYS0500.getStatus());
	}
	
	@ExceptionHandler(SystemErrorException.class)
	public ResponseEntity<BaseResponse> handleSystemException(HttpServletRequest request, SystemErrorException exception) {
		LOGGER.error(stackTrace(exception));
		
		Locale locale = null;
		String acceptLanguage = request.getHeader("Accept-Language");
		if(acceptLanguage != null)
			locale = new Locale(acceptLanguage);	
		Map<String, String> respStatusMessage = new HashMap<String, String>();
		respStatusMessage.put(exception.getErrorCode().name(), errorResponse.errorResponse(exception.getErrorCode(), locale));
		BaseResponse baseResponse = new BaseResponse();
		baseResponse.setRespStatusData("failure");
		baseResponse.setRespStatusMessage(respStatusMessage);
		baseResponse.setError(respStatusMessage.get(exception.getErrorCode().name()));
		return new ResponseEntity<BaseResponse>(baseResponse,
				exception.getErrorCode().getStatus());
	}
	
	private String stackTrace(Exception exception) {
		StringWriter errors = new StringWriter();
		exception.printStackTrace(new PrintWriter(errors));
		return errors.toString();
	}
}
