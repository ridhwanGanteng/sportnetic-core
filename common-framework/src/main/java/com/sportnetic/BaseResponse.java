package com.sportnetic;

import java.util.Map;

import lombok.Data;

/**
 * @author <a href="mailto:agung_putra@sigma.co.id">krisna putra</a>
 *
 * EXAMPLE 1 ON ERROR
 * respStatusData="ERR_SYS001"
 * respStatusMessage=NULL
 * //resource bundle file on Web application
 * ERR_SYS001=INTERNAL SERVER ERROR
 *
 * ------------------------------------------------------------
 *
 * EXAMPLE 2 ON VALIDATION
 * respStatusData="VAL_PAY002
 * respStatusMessage={"#LOAN_NAME":"Krisna","#DEPOSIT":"10000"}
 * //resource bundle file on Web application
 * VAL_PAY002=CUSTOMER #LOAN_NAME ONLY HAVE MONEY #DEPOSIT
 * ------------------------------------------------------------
 *
 *  EXAMPLE 3 ON SUCCESS
 *
 * respStatusData="SUCC_001"
 * respStatusMessage=NULL;
 * //resource bundle file on Web application
 * SUC_001=SUCCESS SAVING DATA
 */
@Data
public class BaseResponse {
   private String error;
   private String respStatusData="success";//default success
   private Map<String,String> respStatusMessage;//default empty
}
