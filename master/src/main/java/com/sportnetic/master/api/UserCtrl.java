package com.sportnetic.master.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.sportnetic.dto.common.DTRequestDto;
import com.sportnetic.dto.common.DTResponseDto;
import com.sportnetic.dto.common.datatables.DTOrder;
import com.sportnetic.feign.dto.master.UserDto;
import com.sportnetic.master.service.UserImplService;

@RestController
@RequestMapping(path = "/api/user", produces = MediaType.APPLICATION_JSON_VALUE)

public class UserCtrl {

	@Autowired
	private UserImplService userService;

	@RequestMapping(value = "/view/get-list-user/v.1", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<DTResponseDto<UserDto>> getListUser(@RequestBody(required = false) DTRequestDto userBody)
			throws Exception {
		return new ResponseEntity<DTResponseDto<UserDto>>(userService.getAllPageAble(userBody), HttpStatus.OK);
	}

	@RequestMapping(value = "/transaction/delete/v.1", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<DTResponseDto<UserDto>> delete(String locale, @RequestBody UserDto... user) throws Exception {
		userService.delete(user);
		return new ResponseEntity<DTResponseDto<UserDto>>(userService.getAllPageAble(null), HttpStatus.OK);
	}

	@RequestMapping(value = "/transaction/insert/v.1", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<DTResponseDto<UserDto>> insert(@RequestBody UserDto user) throws Exception {
		userService.saveOrUpdate(user);
		return new ResponseEntity<DTResponseDto<UserDto>>(userService.getAllPageAble(null), HttpStatus.OK);
	}

}
