package com.sportnetic.master.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.sportnetic.dto.common.DTRequestDto;
import com.sportnetic.dto.common.DTResponseDto;
import com.sportnetic.feign.dto.master.RoleDto;
import com.sportnetic.master.service.RoleImplService;

@RestController
@RequestMapping(path = "/api/role", produces = MediaType.APPLICATION_JSON_VALUE)
public class RoleCtrl {

	@Autowired
	private RoleImplService roleService;

	@RequestMapping(value = "/view/get-list-role/v.1", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<DTResponseDto<RoleDto>> getListUser(@RequestBody(required = false) DTRequestDto roleBody)
			throws Exception {
		return new ResponseEntity<DTResponseDto<RoleDto>>(roleService.getAllPageAble(roleBody), HttpStatus.OK);
	}

	@RequestMapping(value = "/transaction/delete/v.1", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<DTResponseDto<RoleDto>> delete(String locale, @RequestBody RoleDto... roles)
			throws Exception {
		roleService.delete(roles);
		return new ResponseEntity<DTResponseDto<RoleDto>>(roleService.getAllPageAble(null), HttpStatus.OK);
	}

	@RequestMapping(value = "/transaction/insert/v.1", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<DTResponseDto<RoleDto>> insert(@RequestBody RoleDto role) throws Exception {
		roleService.saveOrUpdate(role);
		return new ResponseEntity<DTResponseDto<RoleDto>>(roleService.getAllPageAble(null), HttpStatus.OK);
	}
}
