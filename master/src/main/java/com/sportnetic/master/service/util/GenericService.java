package com.sportnetic.master.service.util;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManagerFactory;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import com.sportnetic.dto.common.DTRequestDto;

import com.sportnetic.dto.common.datatables.DTColumn;
import com.sportnetic.dto.common.datatables.DTOrder;
import com.sportnetic.utils.FilterSpec;
import com.sportnetic.utils.QueryOperator;
public class GenericService {
	@Autowired
	private EntityManagerFactory entityManagerFactory;


	protected Session getSession() {
		if (entityManagerFactory.unwrap(SessionFactory.class) == null) {
			throw new NullPointerException("factory is not a hibernate factory");
		}
		return entityManagerFactory.unwrap(SessionFactory.class).getCurrentSession();
	}

	

	protected Pageable getPageable(DTRequestDto p_dtRequestDtodto) {
		Sort sort = null;
		if (null == p_dtRequestDtodto) {
			return new PageRequest(0, 5);
		}
		int _limit = p_dtRequestDtodto.getLength() > 10 ? (int) p_dtRequestDtodto.getLength() : 5;
		int _page = p_dtRequestDtodto.getStart() / _limit > 0 ? (int) p_dtRequestDtodto.getStart() / _limit : 0;

		if (null != p_dtRequestDtodto.getOrder()) {
			for (DTOrder dtOrder : p_dtRequestDtodto.getOrder()) {
				// Note index 0 is ACTION Column
				if (dtOrder.getColumn() != 0) {
					if (null != p_dtRequestDtodto.getColumns()
							&& p_dtRequestDtodto.getColumns().size() > dtOrder.getColumn()) {
						DTColumn dtColumn = p_dtRequestDtodto.getColumns().get(dtOrder.getColumn());
						if (dtColumn.isOrderable()) {
							Sort.Direction _direction = dtOrder.getDir().equalsIgnoreCase("ASC") ? Sort.Direction.ASC
									: Sort.Direction.DESC;
							if (null == sort) {
								sort = new Sort(_direction, dtColumn.getData());
							} else {
								sort.and(new Sort(_direction, dtColumn.getData()));
							}
						}

					}
				}
			}
		}
		return new PageRequest(_page, _limit, sort);
	}

	

	protected Map<String, FilterSpec> getFilter(DTRequestDto body) {
		Map<String, FilterSpec> filter = new HashMap<String, FilterSpec>();
		FilterSpec value = null;
		int countSearch = 0;
		if (body != null) {
			for (DTColumn column : body.getColumns()) {
				if (!column.getData().isEmpty()) {
					value = new FilterSpec();
					value.setOperator(QueryOperator.LIKE_BOTH_SIDE);
					if (!column.getSearch().getValue().isEmpty()) {
						countSearch++;
						value.setValues(new String[] { column.getSearch().getValue() });
					} else if (!body.getSearch().getValue().isEmpty()) {
						countSearch++;
						value.setValues(new String[] { body.getSearch().getValue() });
					}
					filter.put(column.getData(), value);
				}
			}
			if (countSearch == 0)
				filter = null;
		}
		return filter;
	}

	
	
}
