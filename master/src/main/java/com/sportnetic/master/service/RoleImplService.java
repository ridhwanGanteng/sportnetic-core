package com.sportnetic.master.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.GenericApplicationContext;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import com.sportnetic.dto.common.DTRequestDto;
import com.sportnetic.dto.common.DTResponseDto;
import com.sportnetic.feign.dto.master.RoleDto;
import com.sportnetic.feign.dto.master.UserDto;
import com.sportnetic.master.service.util.GenericService;
import com.sportnetic.repositories.master.dao.RoleRepo;
import com.sportnetic.repositories.master.entity.RoleEntity;
import com.sportnetic.repositories.master.entity.UserEntity;
import com.sportnetic.repositories.master.specification.RoleSpecification;
import com.sportnetic.repositories.master.specification.UserSpecification;
import com.sportnetic.utils.FilterSpec;

@Service("roleImplService")
public class RoleImplService extends GenericService {

	@Autowired
	private RoleRepo roleRepo;

	private RoleEntity roleEntity;
	private RoleDto roleDto;

	public DTResponseDto<RoleDto> getAllPageAble(DTRequestDto roleBody) throws Exception {
		Map<String, FilterSpec> _filter = getFilter(roleBody);
		Page<RoleEntity> _rolePage = roleRepo.findAll(RoleSpecification.getRoleSpec(_filter), getPageable(roleBody));
		DTResponseDto<RoleDto> dataTableResult = new DTResponseDto();
		List<RoleDto> _roleDtos = new ArrayList();
		int no = 0;
		if (roleBody == null) {
			dataTableResult.setDraw(1);
		} else {
			dataTableResult.setDraw(roleBody.getDraw());
			no = (int) roleBody.getStart();
		}

		for (RoleEntity _role : _rolePage.getContent()) {
			RoleDto _roleDto = new RoleDto();
			_roleDto = _role.parseDto();
			_roleDto.setNo(++no);
			_roleDtos.add(_roleDto);

		}

		dataTableResult.setRecordsFiltered(_rolePage.getTotalElements());
		dataTableResult.setRecordsTotal(_roleDtos.size());
		dataTableResult.setData(_roleDtos);
		return dataTableResult;
	}

	public void validateSaveOrUpdate() throws Exception {
		if (this.roleDto == null || this.roleDto.getAuthority() == null || this.roleDto.getAuthority().length() <= 0) {
			throw new Exception();

		}
	}

	public void saveOrUpdate(RoleDto roleDto) throws Exception {
		this.roleDto = roleDto;
		this.validateSaveOrUpdate();
		if (roleDto.getRoleId() != null && roleDto.getRoleId().length() > 0) {
			update();
		} else if (roleDto.getRoleId() == null) {
			insert();
		}
	}

	@Transactional
	private void insert() {
		roleEntity = new RoleEntity(roleDto);
		roleRepo.save(roleEntity);

	}

	@Transactional
	private void update() throws Exception {
		UUID uuid = UUID.fromString(roleDto.getRoleId());
		roleEntity = roleRepo.findByRoleId(uuid);
		if (roleEntity != null) {
			roleEntity.setAuthority(roleDto.getAuthority());
			roleEntity.setDescription(roleDto.getDescription());

			roleRepo.save(roleEntity);
			return;
		}
		throw new Exception();
	}

	private void validateDelete(RoleDto... roles) throws Exception {
		UUID uuid = null;
		for (RoleDto _role : roles) {
			uuid = UUID.fromString(_role.getRoleId());
			RoleEntity _roleEntity = roleRepo.findByRoleId(uuid);
			if (_roleEntity == null || _roleEntity.getIsActive() || _roleEntity.getRoleId() == null) {
				throw new Exception();
			}
		}
	}

	@Transactional
	public void delete(RoleDto... roles) throws Exception {
		validateDelete(roles);
		UUID uuid = null;
		for (RoleDto _roleDto : roles) {
			uuid = UUID.fromString(_roleDto.getRoleId());
			RoleEntity _roleEntity = roleRepo.findByRoleId(uuid);
			_roleEntity.setIsActive(false);

		}

	}
}
