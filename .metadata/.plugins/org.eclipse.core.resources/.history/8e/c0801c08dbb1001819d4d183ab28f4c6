package com.sportnetic.repositories.master.entity;

import java.io.Serializable;
import java.util.List;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sportnetic.repositories.BaseAuditTrailEntity;

import lombok.Data;

@Entity
@Data
@Table(name = "M_VENUE", schema = "SPORT_NETIC_MASTER")
public class VenueEntity extends BaseAuditTrailEntity implements Serializable{/**
	 * 
	 */
	private static final long serialVersionUID = -528183776949755287L;
	@Id
	@GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
	@GeneratedValue(generator = "UUID")
	@Column(name = "VENUE_ID", nullable = false, unique = true)
	private UUID venueId;
	
	@Column(name = "name" )
	private String name;
	
	@Column(name = "LATITUDE" )
	private String latitude;

	@Column(name = "LONGITUDE" )
	private String longitude;
	
	@JsonIgnore
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "R_ROLE", schema = "SPORT_NETIC_MASTER", joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "user_id", updatable = false, nullable = true), inverseJoinColumns = @JoinColumn(name = "MENU_GROUP_ID", referencedColumnName = "MENU_GROUP_ID", updatable = false, nullable = true))
	private List<ThirdpartyAddressEntity> thirdpartyAddress;
	
	@OneToMany(mappedBy = "bank", cascade = CascadeType.ALL)
	public List<ThirdpartyAddressEntity> thirdpartyAddress;

}
