package com.sportnetic.util;

import java.util.Map;

import lombok.Data;

@Data
public class BaseResponse {
	 private String error;
	   private String respStatusData="success";//default success
	   private Map<String,String> respStatusMessage;//default empty
}
