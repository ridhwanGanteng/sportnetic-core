package com.sportnetic.dto.common;

import java.util.List;

import com.sportnetic.dto.common.datatables.DTColumn;
import com.sportnetic.dto.common.datatables.DTOrder;
import com.sportnetic.dto.common.datatables.DTSearch;

import lombok.Data;

@Data
public class DTRequestDto {
	
	private long draw;
	private List<DTColumn> columns;
	private List<DTOrder> order;
	private long start;
	private long length;
	private DTSearch search;

}
