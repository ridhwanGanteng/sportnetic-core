/**
 * 
 */
package com.sportnetic.dto.common;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author <a href="mailto:bagus.sugitayasa@sigma.co.id">GusdeGita</a>
 * @version $Id: TrxRequestDto.java 2783 2018-03-21 07:31:27Z bagus.sugitayasa $ 
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class TrxRequestDto {
	private String id;
	private String[] listOfSelect2;
}
