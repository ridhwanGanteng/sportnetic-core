package com.sportnetic.dto.common;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CoordinateDto implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2905367218393019012L;

	private Double lat;
	private Double lng;

}
