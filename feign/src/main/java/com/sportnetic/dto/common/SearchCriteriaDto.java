package com.sportnetic.dto.common;

import lombok.Data;

import java.util.Map;

/**
 * Created by krisna putra on 12/12/2017.
 */
@Data
public class SearchCriteriaDto {

    private Map<String,Object> query;
    private Integer limit;
    private Integer page;
    private String orderBy;
    private String direction;

}
