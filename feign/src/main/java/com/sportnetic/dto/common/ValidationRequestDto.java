package com.sportnetic.dto.common;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author rudi.sadria
 * @version $Id: ValidationRequestDto.java 1970 2018-02-23 07:29:19Z bagus.sugitayasa $ 
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ValidationRequestDto {
	private String valueToValidate;
}

