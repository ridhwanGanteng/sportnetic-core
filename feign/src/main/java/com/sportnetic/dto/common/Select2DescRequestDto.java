/**
 * 
 */
package com.sportnetic.dto.common;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author <a href="mailto:bagus.sugitayasa@sigma.co.id">GusdeGita</a>
 * @version $Id: Select2DescRequestDto.java 3007 2018-04-10 10:03:52Z bagus.sugitayasa $ 
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Select2DescRequestDto {
	private String[] listOfSelect2;
	Object dto;
	private String fqcn;
}