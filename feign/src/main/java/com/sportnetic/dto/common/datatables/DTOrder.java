package com.sportnetic.dto.common.datatables;

import lombok.Data;

@Data
public class DTOrder {
	
	private int column;
	private String dir;

}
