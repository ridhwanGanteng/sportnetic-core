package com.sportnetic.dto.common.select2;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Select2Dto {
	
	private String id;
	private String text;
	private boolean selected;
	private boolean disabled;
	private List<Select2Dto> children;
	
	

}
