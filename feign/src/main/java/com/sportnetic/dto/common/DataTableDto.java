package com.sportnetic.dto.common;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

import com.sportnetic.util.BaseResponse;


/**
 * Created by krisna putra on 12/12/2017.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DataTableDto<DTO>  extends BaseResponse{
    long total;
    List<DTO> rows;
}
