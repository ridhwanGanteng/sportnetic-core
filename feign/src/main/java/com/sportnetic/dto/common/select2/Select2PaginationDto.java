package com.sportnetic.dto.common.select2;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Select2PaginationDto {
	
	private boolean more;

}
