package com.sportnetic.dto.common;

import java.util.List;

import com.sportnetic.util.BaseResponse;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
public class DTResponseDto<T> extends BaseResponse {
	
	private long draw;
	private long recordsFiltered;
	private long recordsTotal;
	List<T> data;
	private String error;
	
}