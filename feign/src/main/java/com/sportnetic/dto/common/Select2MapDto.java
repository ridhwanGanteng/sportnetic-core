/**
 * 
 */
package com.sportnetic.dto.common;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author <a href="mailto:bagus.sugitayasa@sigma.co.id">GusdeGita</a>
 * @version $Id: Select2MapDto.java 3007 2018-04-10 10:03:52Z bagus.sugitayasa $ 
 */
@Data
@NoArgsConstructor
public class Select2MapDto {

	private String id;
	private String text;
	
	
}
