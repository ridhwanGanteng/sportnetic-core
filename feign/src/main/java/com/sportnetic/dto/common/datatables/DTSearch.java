package com.sportnetic.dto.common.datatables;

import java.util.List;

import lombok.Data;

@Data
public class DTSearch {

	private String value;
	private List<Object> values;
	private boolean regex;
	
}
