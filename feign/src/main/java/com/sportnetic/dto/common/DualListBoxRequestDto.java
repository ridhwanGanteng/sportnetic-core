package com.sportnetic.dto.common;

import lombok.Data;

@Data
public class DualListBoxRequestDto {
	
	private String policyId;
	private String productGroupId;
	private String productId;

}
