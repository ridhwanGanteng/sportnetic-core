package com.sportnetic.dto.common;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

import com.sportnetic.util.BaseResponse;



/**
 * @author agampradhana
 *
 * @param <DTO>
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class GetAllDataResponseDto<DTO>  extends BaseResponse{
    List<DTO> results;
}
