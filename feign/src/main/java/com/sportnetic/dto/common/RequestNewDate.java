/**
 * 
 */
package com.sportnetic.dto.common;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 
 * @author surya_happy
 *
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class RequestNewDate {
	private String newDate;
}
