package com.sportnetic.dto.common;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.sportnetic.util.BaseResponse;

import lombok.Data;

@Data
public class ValidationResponseDto {
	List<BaseResponse> responValidations = new ArrayList<BaseResponse>();
	
	public void addError(String respStatusCode)
	{
		if(responValidations==null)
			responValidations = new ArrayList<BaseResponse>();
		BaseResponse baseResp = new BaseResponse();
		baseResp.setRespStatusData(respStatusCode);
		responValidations.add(baseResp);
	}
	
	public void addError(String respStatusCode,String message)
	{
		if(responValidations==null)
			responValidations = new ArrayList<BaseResponse>();
		BaseResponse baseResp = new BaseResponse();
		baseResp.setRespStatusData(respStatusCode);
		
		Map<String,String> respStatusMessage = new HashMap();
		respStatusMessage.put(respStatusCode, message);
		baseResp.setRespStatusMessage(respStatusMessage);
		responValidations.add(baseResp);
	}
}
