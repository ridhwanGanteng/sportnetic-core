package com.sportnetic.dto;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;

public abstract class GenericDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4010133534085767972L;

	public static final String SYSTEM_DATE_PATTERN = "yyyy-MM-dd HH:mm:ss";

	protected Boolean isActive;
	protected String strIsActive;
	protected String createdBy;
	protected String createdDate;
	protected String modifiedBy;
	protected String modifiedDate;
	protected String description;
	protected int no;

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreatedDate() {
		return createdDate;
	}
	public void setNo(int no) {
		this.no = no;
	}
	public int getNo() {
		return no;
	}

	@JsonIgnore
	public Date getCreatedDateParse() {
		try {
			if (createdDate != null)
				return new SimpleDateFormat(SYSTEM_DATE_PATTERN).parse(this.createdDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return new Date();
	}

	public void setCreatedDate(Date date) {
		if (date == null)
			this.createdDate = "";
		else
			this.createdDate = new SimpleDateFormat(SYSTEM_DATE_PATTERN).format(date);
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public String getModifiedDate() {
		return modifiedDate;
	}

	@JsonIgnore
	public Date getModifiedDateParse() {
		try {
			if (modifiedDate != null)
				return new SimpleDateFormat(SYSTEM_DATE_PATTERN).parse(this.modifiedDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return new Date();
	}

	public void setModifiedDate(Date date) {
		if (date == null)
			this.modifiedDate = "";
		else
			this.modifiedDate = new SimpleDateFormat(SYSTEM_DATE_PATTERN).format(date);
	}

	public void setModifiedDate(String modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
		setStrIsActive();
	}

	public String getStrIsActive() {
		return strIsActive;
	}

	public void setStrIsActive(String strIsActive) {
		this.strIsActive = strIsActive;
	}

	public void setStrIsActive() {
		this.strIsActive = (this.isActive) ? "Active" : "Inactive";
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDescription() {
		return description;
	}

}
