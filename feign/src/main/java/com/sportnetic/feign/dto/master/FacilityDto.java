package com.sportnetic.feign.dto.master;

import java.io.Serializable;
import java.util.List;

import com.sportnetic.dto.GenericDto;

import lombok.Data;

@Data
public class FacilityDto extends GenericDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3332461237256331945L;
	private String facilityId;
	private Boolean waitingArea;
	private Boolean parkingArea;
	private Boolean pantri;
	private Boolean toilet;
	private Boolean musholah;
	private Boolean other;
	
	
}
