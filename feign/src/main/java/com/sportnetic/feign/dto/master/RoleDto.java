package com.sportnetic.feign.dto.master;

import java.io.Serializable;
import java.util.List;

import com.sportnetic.dto.GenericDto;

import lombok.Data;

@Data
public class RoleDto extends GenericDto implements Serializable {
	private String roleId;
	private String authority;
	private List<MenuGroupDto> menuGroup;
}
