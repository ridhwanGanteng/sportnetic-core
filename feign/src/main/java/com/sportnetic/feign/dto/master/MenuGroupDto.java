package com.sportnetic.feign.dto.master;

import java.io.Serializable;
import java.util.List;

import com.sportnetic.dto.GenericDto;

import lombok.Data;

@Data
public class MenuGroupDto extends GenericDto implements Serializable {

	private static final long serialVersionUID = 1L;
	private String menuId;
	private String label;
	private String icon;
	private List<MenuDto> menu;
}
