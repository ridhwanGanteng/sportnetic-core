package com.sportnetic.feign.dto.master;

import java.io.Serializable;

import com.sportnetic.dto.GenericDto;

import lombok.Data;

@Data
public class ThirdpartyAddressDto extends GenericDto implements Serializable {
	private String thirdpartyAddressId;
	private String thirdpartyId;
	private String postalCode;
	private String detailAddress;
}
