package com.sportnetic.feign.dto.master;

import java.io.Serializable;

import com.sportnetic.dto.GenericDto;

import lombok.Data;

@Data
public class ThirdPartyPhotoDto extends GenericDto implements Serializable{/**
	 * 
	 */
	private static final long serialVersionUID = 1656002190334745156L;
	private String thirdpartyId;
	private String thirdpartyPhotoId;
	private String type;
	private String value;
}
