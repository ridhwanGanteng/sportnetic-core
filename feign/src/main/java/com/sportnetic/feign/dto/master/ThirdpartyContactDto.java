package com.sportnetic.feign.dto.master;

import java.io.Serializable;

import com.sportnetic.dto.GenericDto;

import lombok.Data;

@Data
public class ThirdpartyContactDto extends GenericDto implements Serializable {/**
	 * 
	 */
	private static final long serialVersionUID = 5252062815506079507L;
	private String thirdpartyId;
	private String thirdpartyContactId;
	private String type;
	private String value;

}
