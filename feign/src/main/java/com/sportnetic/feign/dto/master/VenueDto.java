package com.sportnetic.feign.dto.master;

import java.io.Serializable;
import java.util.List;

import com.sportnetic.dto.GenericDto;

import lombok.Data;

@Data
public class VenueDto extends GenericDto implements Serializable{/**
	 * 
	 */
	private static final long serialVersionUID = 8367977973211871339L;

	private String venueId;
	private String name;
	private String latitude;
	private String longitude;
	
	private List<ThirdpartyAddressDto> address;
	private List<ThirdpartyContactDto> contact;
	private List<ThirdPartyPhotoDto> photo;
	private List<ArenaDto> arena;
	private FacilityDto facility;
	

	
}
