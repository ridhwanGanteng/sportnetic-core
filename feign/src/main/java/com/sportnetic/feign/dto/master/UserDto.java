package com.sportnetic.feign.dto.master;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.sportnetic.dto.GenericDto;

import lombok.Data;

@Data
public class UserDto extends GenericDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private String userId;

	private String email;

	private String nama;

	private Date birthDate;

	private String password;

	private String genderLabel;
	private String gender;
	private String address;
	private String phoneNumber;

	private List<RoleDto> roles;

	public String getGenderLabel() {
		return this.gender.equalsIgnoreCase("L") ? "Pria" : "Wanita";
	}

}
