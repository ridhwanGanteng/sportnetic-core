package com.sportnetic.feign.dto.master;

import java.io.Serializable;

import com.sportnetic.dto.GenericDto;

import lombok.Data;

@Data
public class ArenaDto extends GenericDto implements Serializable{/**
	 * 
	 */
	private static final long serialVersionUID = 5612026117539183073L;

	private String arenaId;
	private String name;
	private String arenaTypeId;
	private String dimensions;
	private Integer price;
	private String categoryId;
	private String arenaSpec;
}
