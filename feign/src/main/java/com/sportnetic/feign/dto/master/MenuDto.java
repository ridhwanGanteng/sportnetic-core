package com.sportnetic.feign.dto.master;

import java.io.Serializable;

import com.sportnetic.dto.GenericDto;

import lombok.Data;

@Data
public class MenuDto extends GenericDto implements Serializable{/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String menuId;
	private String label;
	private String icon;
	private String url;

}
